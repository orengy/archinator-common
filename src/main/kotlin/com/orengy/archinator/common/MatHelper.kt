package com.orengy.archinator.common

import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.google.gson.Gson
import com.google.gson.JsonElement
import org.opencv.core.*
import org.opencv.imgcodecs.Imgcodecs
import org.opencv.imgproc.Imgproc
import java.util.*


object MatHelper {

    fun angleDifference(a: Int, b: Int, range: Int): Int {
        val d = a - b
        return (d + range) % (range * 2) - range
    }

    fun distance(a: Point, b: Point): Double {
        return Math.sqrt(Math.pow(a.x - b.x, 2.0) + Math.pow(a.y - b.y, 2.0))
    }

    fun eccentricity(e: RotatedRect): Double {
        val a = Math.max(e.size.width, e.size.height)
        val b = Math.min(e.size.width, e.size.height)
        return Math.sqrt(Math.pow(a, 2.0) - Math.pow(b, 2.0)) / a
    }

    fun resize(src: Mat, width: Int = 768): Size {
        val size = src.size()
        val height = (size.height * width) / size.width

        Imgproc.resize(src, src, Size(width.toDouble(), height))
        return size.clone()
    }

    val targetColors = listOf<Triple<Color, Scalar, Scalar>>(
            Triple(Color.WHITE, Scalar(0.0, 0 * 2.55, 40 * 2.55), Scalar(179.0, 20 * 2.55, 100 * 2.55)),
            Triple(Color.BLACK, Scalar(0.0, 0 * 2.55, 0 * 2.55), Scalar(179.0, 255 * 2.55, 40 * 2.55)),
            Triple(Color.RED, Scalar(0.0, 20 * 2.55, 40 * 2.55), Scalar(14.0, 255 * 2.55, 100 * 2.55)),
            Triple(Color.RED, Scalar(167.0, 20 * 2.55, 40 * 2.55), Scalar(179.0, 255 * 2.55, 100 * 2.55)),
            Triple(Color.PURPLE, Scalar(131.0, 20 * 2.55, 40 * 2.55), Scalar(167.0, 255 * 2.55, 100 * 2.55)),
            Triple(Color.BLUE, Scalar(86.0, 20 * 2.55, 40 * 2.55), Scalar(131.0, 255 * 2.55, 100 * 2.55)),
            Triple(Color.GREEN, Scalar(38.0, 20 * 2.55, 40 * 2.55), Scalar(86.0, 255 * 2.55, 100 * 2.55)),
            Triple(Color.YELLOW, Scalar(14.0, 20 * 2.55, 40 * 2.55), Scalar(38.0, 255 * 2.55, 100 * 2.55))
    )

    fun extractColor(src: Mat, target: Color): Mat {


        val hsv = Mat()
        Imgproc.cvtColor(src, hsv, Imgproc.COLOR_BGR2HSV)


        for ((c, min, max) in targetColors) {
            if (target.isTheSameColor(c)) {
                val mask = Mat()
                Core.inRange(hsv, min, max, mask)
                Imgproc.cvtColor(mask, mask, Imgproc.COLOR_GRAY2BGR)
                return mask
            }
        }

        val res2 = src.clone()
        res2.setTo(Color.BLACK.asBGRScalar())
        return res2

    }

    fun closestColors(src: Mat): Mat {
        val res = src.clone()
        val hsv = Mat()
        val temp = src.clone()
        Imgproc.cvtColor(src, hsv, Imgproc.COLOR_BGR2HSV)

//        val colors = Colors.colors

        for ((c, min, max) in targetColors) {
            val mask = Mat()
            temp.setTo(c.asBGRScalar())
            Core.inRange(hsv, min, max, mask)
            temp.copyTo(res, mask)
        }
        return res
    }

    fun matToJson(mat: Mat): JsonElement {
        val obj = JsonObject()

        if (mat.isContinuous) {
            val cols = mat.cols()
            val rows = mat.rows()
            val elemSize = mat.elemSize().toInt()

            val data = ByteArray(cols * rows * elemSize)

            mat.get(0, 0, data)

            obj.addProperty("rows", mat.rows())
            obj.addProperty("cols", mat.cols())
            obj.addProperty("type", mat.type())

            // We cannot set binary data to a json object, so:
            // Encoding data byte array to Base64.
            val dataString = Base64.getEncoder().encodeToString(data)

            obj.addProperty("data", dataString)

            return obj
        }
        return JsonObject()
    }

    fun matFromJson(json: String): Mat {
        val parser = JsonParser()
        val JsonObject = parser.parse(json).asJsonObject

        val rows = JsonObject.get("rows").asInt
        val cols = JsonObject.get("cols").asInt
        val type = JsonObject.get("type").asInt

        val dataString = JsonObject.get("data").asString
        val data = Base64.getDecoder().decode(dataString.toByteArray())

        val mat = Mat(rows, cols, type)
        mat.put(0, 0, data)

        return mat
    }


    fun convertIndexesToPoints(contour: MatOfPoint, indexes: MatOfInt): MatOfPoint {
        val arrIndex = indexes.toArray()
        val arrContour = contour.toArray()
        val arrPoints = arrayOfNulls<Point>(arrIndex.size)

        for (i in arrIndex.indices) {
            arrPoints[i] = arrContour[arrIndex[i]]
        }

        val hull = MatOfPoint()
        hull.fromArray(*arrPoints)
        return hull
    }

    fun perspectivePointTransform(p: Point, perspective: Mat): Point {
        val dst = Mat()
        val src = Mat(1, 1, CvType.CV_32FC2)
        src.put(0, 0, p.x, p.y)
        Core.perspectiveTransform(src, dst, perspective)

        return Point(dst.get(0, 0)[0], dst.get(0, 0)[1])
    }
}