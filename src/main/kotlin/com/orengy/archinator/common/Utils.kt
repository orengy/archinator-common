package com.orengy.archinator.common

import java.io.InputStream
import java.io.ByteArrayOutputStream
import java.nio.charset.StandardCharsets
import java.io.FileInputStream


/**
 * Created by Adomas on 2017-11-21.
 */
object Utils {
    fun streamToString(stream: InputStream): String {
        val result = ByteArrayOutputStream()
        val buffer = ByteArray(1024)
        var length: Int = stream.read(buffer)
        while (length != -1) {
            result.write(buffer, 0, length)
            length = stream.read(buffer)
        }

        return result.toString(StandardCharsets.UTF_8.name())
    }

    fun fileContents(filePath: String): String {
        val fis = FileInputStream(filePath)
        val res = streamToString(fis)
        fis.close()
        return res
    }
}