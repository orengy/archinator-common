package com.orengy.archinator.common

import com.google.gson.*
import org.opencv.core.*
import java.util.ArrayList
import org.opencv.imgcodecs.Imgcodecs
import java.io.File
import org.opencv.imgproc.Imgproc
import org.opencv.calib3d.Calib3d
import org.opencv.core.MatOfPoint2f
import org.opencv.core.TermCriteria


class Camera(val numCornersHor: Int,
             val numCornersVer: Int) {
    var isCalibrated = false
        private set

    private var imagePoints = mutableListOf<Mat>()
    private val objectPoints = mutableListOf<Mat>()
    private val obj: MatOfPoint3f = MatOfPoint3f()
    private var imageCorners: MatOfPoint2f = MatOfPoint2f()

    private var rvecs = mutableListOf<Mat>()
    private var tvecs = mutableListOf<Mat>()

    private var successes: Int = 0
    private var intrinsic: Mat = Mat(3, 3, CvType.CV_32FC1)
    private var distCoeffs: Mat = Mat(1, 5, CvType.CV_32FC1)

    init {
        intrinsic.put(0, 0, 1.0)
        intrinsic.put(1, 1, 1.0)

        for (j in 0 until numCornersVer)
            for (i in 0 until numCornersHor)
                obj.push_back(MatOfPoint3f(Point3((i).toDouble(), (j).toDouble(), 0.0)))
    }

    private val gson = GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create()

    fun print() {
        println(intrinsic.dump())
        println(distCoeffs.dump())
    }

    fun toJsonString(): String {
        val json = JsonObject()
        val intrinsicArrayJson = JsonArray()

        for (i in 0..2) {
            for (j in 0..2) {
                intrinsicArrayJson.add(intrinsic.get(i, j)[0])
            }
        }
        val distCoeffsArrayJson = JsonArray()
        for (i in 0..4) {
            distCoeffsArrayJson.add(distCoeffs.get(0, i)[0])
        }

        json.add("intrinsic", intrinsicArrayJson)
        json.add("dist_coeffs", distCoeffsArrayJson)

        return json.toString()
    }

    fun fromJsonString(jsonString: String) {
        val parser = JsonParser()
        val json = parser.parse(jsonString)
        val intrinsicData = json.asJsonObject.get("intrinsic").asJsonArray.map { it.asDouble }
        intrinsic.put(0, 0, *intrinsicData.toDoubleArray())
        val distCoeffsData = json.asJsonObject.get("dist_coeffs").asJsonArray.map { it.asDouble }
        distCoeffs.put(0, 0, *distCoeffsData.toDoubleArray())
        isCalibrated = true
    }

    fun calibrate(path: String) {
        if (isCalibrated)
            return

        val folder = File(path)
        val listOfFiles = folder.listFiles()
        var imgSize = Size()
        for ((i, file) in listOfFiles.withIndex()) {
            val imgColor = Imgcodecs.imread(file.path);
            MatHelper.resize(imgColor)
            val img = Mat()
            Imgproc.cvtColor(imgColor, img, Imgproc.COLOR_BGR2GRAY);
            val boardSize = Size(numCornersHor.toDouble(), numCornersVer.toDouble())
            val found = Calib3d.findChessboardCorners(img, boardSize, imageCorners, Calib3d.CALIB_CB_ADAPTIVE_THRESH + Calib3d.CALIB_CB_NORMALIZE_IMAGE + Calib3d.CALIB_CB_FAST_CHECK)

            if (found) {
                val term = TermCriteria(TermCriteria.EPS or TermCriteria.MAX_ITER, 30, 0.1)
                Imgproc.cornerSubPix(img, imageCorners, Size(11.0, 11.0), Size(-1.0, -1.0), term)

                imgSize = img.size()

                this.imagePoints.add(imageCorners)
                imageCorners = MatOfPoint2f()
                this.objectPoints.add(obj)
                this.successes++
            }
        }

        Calib3d.calibrateCamera(objectPoints, imagePoints, imgSize, intrinsic, distCoeffs, rvecs, tvecs)
        isCalibrated = true


    }

    fun calibrate(images: List<Mat>) {
        if (isCalibrated)
            return
        var imgSize = Size()
        for (img in images) {
            val boardSize = Size(numCornersHor.toDouble(), numCornersVer.toDouble())
            val found = Calib3d.findChessboardCorners(img, boardSize, imageCorners, Calib3d.CALIB_CB_ADAPTIVE_THRESH + Calib3d.CALIB_CB_NORMALIZE_IMAGE + Calib3d.CALIB_CB_FAST_CHECK)

            if (found) {
                val term = TermCriteria(TermCriteria.EPS or TermCriteria.MAX_ITER, 30, 0.1)
                Imgproc.cornerSubPix(img, imageCorners, Size(11.0, 11.0), Size(-1.0, -1.0), term)

                imgSize = img.size()

                this.imagePoints.add(imageCorners)
                imageCorners = MatOfPoint2f()
                this.objectPoints.add(obj)
                this.successes++
            }
        }

        Calib3d.calibrateCamera(objectPoints, imagePoints, imgSize, intrinsic, distCoeffs, rvecs, tvecs)
        isCalibrated = true


    }

    fun undistort(original: Mat, undistorted: Mat) {
        if (!isCalibrated) {
            original.copyTo(undistorted)
            return
        }

        Imgproc.undistort(original, undistorted, intrinsic, distCoeffs);
    }
}