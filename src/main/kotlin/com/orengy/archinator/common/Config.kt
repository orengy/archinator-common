package com.orengy.archinator.common

import org.opencv.core.Point

object Config {
    object Target {
        val TIER_GOLD = 1
        val TIER_RED = 2
        val TIER_BLUE = 3
        val TIER_BLACK = 4
        val TIER_WHITE = 5

        val TARGET_RADIUS = 300.0
        val TARGET_PADDING = 50.0

        val TARGET_IMAGE_SIZE = (TARGET_RADIUS + TARGET_PADDING) * 2
        val TARGET_CENTER = Point(TARGET_RADIUS + TARGET_PADDING, TARGET_RADIUS + TARGET_PADDING)
    }
}