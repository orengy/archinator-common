package com.orengy.archinator.common

import com.orengy.archinator.common.logger.AbstractLogger
import com.orengy.archinator.common.logger.EmptyLogger
import org.opencv.calib3d.Calib3d
import org.opencv.core.*
import org.opencv.features2d.FeatureDetector
import org.opencv.imgcodecs.Imgcodecs
import org.opencv.imgproc.Imgproc
import org.opencv.features2d.Features2d
import org.opencv.core.MatOfPoint2f
import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.features2d.DescriptorExtractor
import org.opencv.features2d.DescriptorMatcher
import java.util.*
import org.opencv.core.Core


class TargetDetector(
        val img: Mat,
        val targetScores: List<Triple<Int, Double, Double>> = listOf(
                Triple(10, 0.0, 0.10),
                Triple(9, 0.10, 0.20),
                Triple(8, 0.20, 0.30),
                Triple(7, 0.30, 0.40),
                Triple(6, 0.40, 0.50),
                Triple(5, 0.50, 0.60),
                Triple(4, 0.60, 0.70),
                Triple(3, 0.70, 0.80),
                Triple(2, 0.80, 0.90),
                Triple(1, 0.90, 1.0),
                Triple(0, 1.0, 1000.0)
        ),
        val targetDiameter: Double = 0.6,
        val arrowDiameterMm: Double = 1.0,
        val referenceTarget: TargetDetector? = null,
        val logger: AbstractLogger = EmptyLogger()) {

    val closest = MatHelper.closestColors(img)

    private val arrowDiameterPixels: Double = arrowDiameterMm * Config.Target.TARGET_RADIUS / (1000.0 * targetDiameter)

    private lateinit var bestFittedTarget: FitTarget

    lateinit var perspective: Mat
    lateinit var inversePerspective: Mat
    var angleToReferenceTarget: Double = 0.0

    init {
        detectTarget()

        calculateAngleToReferenceTarget()

        fixTargetRings()



        logger.log("perspectiveFixed", getTransformedTarget())
    }

    private fun calculateAngleToReferenceTarget() {
        if (referenceTarget != null) {
            val sceneImage = referenceTarget.img
            val objectImage = img


            val objectKeyPoints = MatOfKeyPoint()
            val featureDetector = FeatureDetector.create(FeatureDetector.ORB)
            featureDetector.detect(objectImage, objectKeyPoints)
            val keypoints = objectKeyPoints.toArray()

            val objectDescriptors = MatOfKeyPoint()
            val descriptorExtractor = DescriptorExtractor.create(DescriptorExtractor.ORB)
            descriptorExtractor.compute(objectImage, objectKeyPoints, objectDescriptors)

            val outputImage = Mat(objectImage.rows(), objectImage.cols(), Imgcodecs.CV_LOAD_IMAGE_COLOR)
            val newKeypointColor = Scalar(255.0, 0.0, 0.0)

            Features2d.drawKeypoints(objectImage, objectKeyPoints, outputImage, newKeypointColor, 0)

            val sceneKeyPoints = MatOfKeyPoint()
            val sceneDescriptors = MatOfKeyPoint()
            featureDetector.detect(sceneImage, sceneKeyPoints)
            descriptorExtractor.compute(sceneImage, sceneKeyPoints, sceneDescriptors)

            val outputImageScene = Mat(sceneImage.rows(), sceneImage.cols(), Imgcodecs.CV_LOAD_IMAGE_COLOR)
            val newKeypointColorScene = Scalar(255.0, 0.0, 0.0)

            Features2d.drawKeypoints(sceneImage, sceneKeyPoints, outputImageScene, newKeypointColorScene, 0)

            val matchoutput = Mat(sceneImage.rows() * 2, sceneImage.cols() * 2, Imgcodecs.CV_LOAD_IMAGE_COLOR)
            val matchestColor = Scalar(0.0, 255.0, 0.0)


            val matches = LinkedList<MatOfDMatch>()
            val descriptorMatcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE)
            descriptorMatcher.knnMatch(objectDescriptors, sceneDescriptors, matches, 2)

            val goodMatchesList = LinkedList<DMatch>()

            val nndrRatio = 0.7f

            for (i in matches.indices) {
                val matofDMatch = matches[i]
                val dmatcharray = matofDMatch.toArray()
                val m1 = dmatcharray[0]
                val m2 = dmatcharray[1]

                if (m1.distance <= m2.distance * nndrRatio) {
                    goodMatchesList.addLast(m1)

                }
            }
            println(goodMatchesList.size)
            if (goodMatchesList.size >= 7) {

                val objKeypointlist = objectKeyPoints.toList()
                val scnKeypointlist = sceneKeyPoints.toList();

                val objectPoints = mutableListOf<Point>()
                val scenePoints = mutableListOf<Point>()


                for (i in 0 until goodMatchesList.size) {
                    objectPoints.add(objKeypointlist[goodMatchesList[i].queryIdx].pt)
                    scenePoints.add(scnKeypointlist[goodMatchesList[i].trainIdx].pt)
                }

                val objMatOfPoint2f = MatOfPoint2f()
                objMatOfPoint2f.fromList(objectPoints)
                val scnMatOfPoint2f = MatOfPoint2f()
                scnMatOfPoint2f.fromList(scenePoints)

                val referenceTargetPerspectiveInverse = Calib3d.findHomography(scnMatOfPoint2f, objMatOfPoint2f, Calib3d.RANSAC, 3.0)

                val circlePoints = pointsAroundTheCircle(Config.Target.TARGET_CENTER, Config.Target.TARGET_RADIUS, 20)
                val angles = mutableListOf<Double>()
                for (p in circlePoints) {
                    val p1 = referenceTarget.inversePerspectivePointTransform(p)

                    val tp = bestFittedTarget.perspectivePointTransform(MatHelper.perspectivePointTransform(p1, referenceTargetPerspectiveInverse))


                    val angle = LineHelper.angleBetweenVectors(
                            Point(
                                    p1.x - Config.Target.TARGET_CENTER.x,
                                    p1.y - Config.Target.TARGET_CENTER.y
                            ),
                            Point(
                                    tp.x - Config.Target.TARGET_CENTER.x,
                                    tp.y - Config.Target.TARGET_CENTER.y
                            )
                    )
                    angles.add(angle)
                }

                angleToReferenceTarget = angles.average()

                println(angleToReferenceTarget)
            } else {
                println("HEEEEEY")
            }

        }
    }

    private fun fixTargetRings() {
        run {
            val front = Mat()
            Imgproc.warpPerspective(img, front, bestFittedTarget.perspective, Size(Target.TARGET_IMAGE_SIZE, Target.TARGET_IMAGE_SIZE))
            val frontClosest = MatHelper.closestColors(front)

            val tierCircles = mutableListOf<Triple<Int, Point, Int>>()

            val dilationSize = 10.0
            val element = Imgproc.getStructuringElement(Imgproc.MORPH_CROSS, Size(dilationSize, dilationSize))



            for (i in 1..4) {
                val color = Color.tierColor(i);

                val mask = Mat(front.size(), front.type())
                mask.setTo(Color.BLACK.asBGRScalar())

                Imgproc.circle(
                        mask,
                        Target.TARGET_CENTER,
                        (Target.TARGET_RADIUS * (i + 0.5) / Target.TIER_WHITE).toInt(),
                        Color.WHITE.asBGRScalar(), -1)
                val tierExtracted = MatHelper.extractColor(frontClosest, color)
                val tierOnly = Mat()

                tierExtracted.copyTo(tierOnly, mask)


                Imgproc.circle(
                        tierOnly,
                        Target.TARGET_CENTER,
                        (Target.TARGET_RADIUS * (i - 0.5) / Target.TIER_WHITE).toInt(),
                        Color.WHITE.asBGRScalar(), -1)

                Imgproc.dilate(tierOnly, tierOnly, element)
                Imgproc.erode(tierOnly, tierOnly, element)

//                logger.log("mask$i", tierOnly)
                val t = 20.0
                val circles = Mat()

                Imgproc.cvtColor(tierOnly, tierOnly, Imgproc.COLOR_BGR2GRAY)


                Imgproc.blur(tierOnly, tierOnly, Size(9.0, 9.0))


                Imgproc.HoughCircles(tierOnly, circles,
                        Imgproc.CV_HOUGH_GRADIENT,
                        1.0,
                        100.1,
                        t * 3,
                        t,
                        (Target.TARGET_RADIUS * (i - 0.3) / Target.TIER_WHITE).toInt(),
                        (Target.TARGET_RADIUS * (i + 0.3) / Target.TIER_WHITE).toInt())

                val circlesList = mutableListOf<Pair<Point, Int>>()

                if (circles.cols() > 0) {
                    for (x in 0 until circles.cols()) {
                        val vCircle = circles.get(0, x) ?: break

                        val pt = Point(vCircle[0], vCircle[1])
                        val radius = Math.round(vCircle[2]).toInt()
                        circlesList.add(Pair(pt, radius))
                        Imgproc.circle(front, pt, radius, Color.RED.asBGRScalar(), 2)
                    }
                } else {
                    continue
                }

                val (c, r) = circlesList.minBy {
                    //                LineHelper.distanceBetweenPoints(it.first, Target.TARGET_CENTER)+
                    Math.abs((Target.TARGET_RADIUS * i / Target.TIER_WHITE).toInt() - it.second)
                }!!
                tierCircles.add(Triple(i, c, r))

                Imgproc.circle(front, c, r, Color.GREEN.asBGRScalar(), 2)
            }

            val sourcePoints = mutableListOf<Point>()
            val destinationPoints = mutableListOf<Point>()
            val ellipsePoints = mutableListOf<Point>()

            for ((t, c, r) in tierCircles) {

                val originalPoints = MatOfPoint2f()
//                cnt.
//                hullPoints.convertTo(cnt, CvType.CV_32F);
//
//                val ellipse = Imgproc.fitEllipse(cnt)

                for (i in 0..360 step 5) {
                    val x = r * Math.cos(Math.toRadians(i.toDouble())) + c.x
                    val y = r * Math.sin(Math.toRadians(i.toDouble())) + c.y

                    ellipsePoints.add(
                            bestFittedTarget.inversePerspectivePointTransform(Point(x, y))
                    )

                    destinationPoints.add(
                            Point(
                                    Config.Target.TARGET_CENTER.x + Config.Target.TARGET_RADIUS * t * Math.cos(Math.toRadians(i.toDouble() + angleToReferenceTarget)) / Config.Target.TIER_WHITE,
                                    Config.Target.TARGET_CENTER.y + Config.Target.TARGET_RADIUS * t * Math.sin(Math.toRadians(i.toDouble() + angleToReferenceTarget)) / Config.Target.TIER_WHITE
                            )
                    )
                }


                originalPoints.fromList(ellipsePoints)
                val e = Imgproc.minAreaRect(originalPoints)
                val a = Math.toRadians(e.angle)

                for (i in 0..360 step 5) {
                    val x = e.size.width * Math.cos(Math.toRadians(i - e.angle)) / 2
                    val y = e.size.height * Math.sin(Math.toRadians(i - e.angle)) / 2
                    sourcePoints.add(
                            Point(
                                    e.center.x + (x * Math.cos(a) - y * Math.sin(a)),
                                    e.center.y + (x * Math.sin(a) + y * Math.cos(a))
                            )
                    )
                }
            }


            sourcePoints.add(bestFittedTarget.inversePerspectivePointTransform(Target.TARGET_CENTER))
            destinationPoints.add(Target.TARGET_CENTER)

            val perspectiveSourcePoints = MatOfPoint2f()
            perspectiveSourcePoints.fromList(sourcePoints)
            val perspectiveDestinationPoints = MatOfPoint2f()
            perspectiveDestinationPoints.fromList(destinationPoints)

            perspective = Calib3d.findHomography(perspectiveSourcePoints, perspectiveDestinationPoints)
            inversePerspective = Calib3d.findHomography(perspectiveDestinationPoints, perspectiveSourcePoints)
        }
        return
        val blurred = Mat()
        Imgproc.blur(img, blurred, Size(2.0, 2.0))
        Imgproc.warpPerspective(blurred, blurred, bestFittedTarget.perspective, Size(Target.TARGET_IMAGE_SIZE, Target.TARGET_IMAGE_SIZE))
        Imgproc.cvtColor(blurred, blurred, Imgproc.COLOR_BGR2GRAY)

        Imgproc.Canny(blurred, blurred, 20.0, 60.0)


        Imgproc.dilate(blurred, blurred, Imgproc.getStructuringElement(Imgproc.MORPH_CROSS, Size(3.0, 3.0)))
        Imgproc.erode(blurred, blurred, Imgproc.getStructuringElement(Imgproc.MORPH_CROSS, Size(3.0, 3.0)))

        val perspectiveImg = Mat()
        Imgproc.warpPerspective(closest, perspectiveImg, bestFittedTarget.perspective, Size(Target.TARGET_IMAGE_SIZE, Target.TARGET_IMAGE_SIZE))
        val temp = Mat()
        Imgproc.warpPerspective(img, temp, bestFittedTarget.perspective, Size(Target.TARGET_IMAGE_SIZE, Target.TARGET_IMAGE_SIZE))
        Imgproc.cvtColor(perspectiveImg, perspectiveImg, Imgproc.COLOR_BGR2GRAY)
        val t = 20.0
        Imgproc.Canny(perspectiveImg, perspectiveImg, t, t * 3)

        val dilationSize = 3.0
        val element = Imgproc.getStructuringElement(Imgproc.MORPH_CROSS, Size(dilationSize, dilationSize))
        Imgproc.dilate(perspectiveImg, perspectiveImg, element)


//        logger.log("borders", perspectiveImg)

        val tierCircles = mutableListOf<Triple<Int, Point, Int>>()

        for (i in 1..5) {
            val mask = Mat(perspectiveImg.size(), perspectiveImg.type())
            mask.setTo(Color.BLACK.asBGRScalar())

            Imgproc.circle(
                    mask,
                    Target.TARGET_CENTER,
                    (Target.TARGET_RADIUS * (i + 0.15) / Target.TIER_WHITE).toInt(),
                    Color.WHITE.asBGRScalar(), -1)
            Imgproc.circle(
                    mask,
                    Target.TARGET_CENTER,
                    (Target.TARGET_RADIUS * (i - 0.15) / Target.TIER_WHITE).toInt(),
                    Color.BLACK.asBGRScalar(), -1)

            val bord = Mat()

            blurred.copyTo(bord, mask)

            logger.log("mask$i", bord)
            val circles = Mat()
            Imgproc.HoughCircles(bord, circles,
                    Imgproc.CV_HOUGH_GRADIENT,
                    1.0,
                    100.1,
                    t * 3,
                    t,
                    (Target.TARGET_RADIUS * (i - 0.3) / Target.TIER_WHITE).toInt(),
                    (Target.TARGET_RADIUS * (i + 0.3) / Target.TIER_WHITE).toInt())

            val circlesList = mutableListOf<Pair<Point, Int>>()

            if (circles.cols() > 0) {
                for (x in 0 until circles.cols()) {
                    val vCircle = circles.get(0, x) ?: break

                    val pt = Point(vCircle[0], vCircle[1])
                    val radius = Math.round(vCircle[2]).toInt()
                    circlesList.add(Pair(pt, radius))
                    Imgproc.circle(temp, pt, radius, Color.RED.asBGRScalar(), 2)
                }
            } else {
                continue
            }

            val (c, r) = circlesList.minBy {
                //                LineHelper.distanceBetweenPoints(it.first, Target.TARGET_CENTER)+
                Math.abs((Target.TARGET_RADIUS * i / Target.TIER_WHITE).toInt() - it.second)
            }!!
            tierCircles.add(Triple(i, c, r))

            Imgproc.circle(temp, c, r, Color.GREEN.asBGRScalar(), 2)
        }


        logger.log("rings", temp)

        val sourcePoints = mutableListOf<Point>()
        val destinationPoints = mutableListOf<Point>()

        for ((t, c, r) in tierCircles) {
            for (i in 0..360 step 5) {
                val x = r * Math.cos(Math.toRadians(i.toDouble())) + c.x
                val y = r * Math.sin(Math.toRadians(i.toDouble())) + c.y


                sourcePoints.add(
                        bestFittedTarget.inversePerspectivePointTransform(Point(x, y))
                )

                destinationPoints.add(
                        Point(
                                Config.Target.TARGET_PADDING + Config.Target.TARGET_RADIUS + Config.Target.TARGET_RADIUS * t * Math.cos(Math.toRadians(i.toDouble())) / Config.Target.TIER_WHITE,
                                Config.Target.TARGET_PADDING + Config.Target.TARGET_RADIUS + Config.Target.TARGET_RADIUS * t * Math.sin(Math.toRadians(i.toDouble())) / Config.Target.TIER_WHITE
                        )
                )
            }
        }

        sourcePoints.add(bestFittedTarget.inversePerspectivePointTransform(Target.TARGET_CENTER))
        destinationPoints.add(Target.TARGET_CENTER)

        val perspectiveSourcePoints = MatOfPoint2f()
        perspectiveSourcePoints.fromArray(*sourcePoints.toTypedArray())
        val perspectiveDestinationPoints = MatOfPoint2f()
        perspectiveDestinationPoints.fromArray(*destinationPoints.toTypedArray())

        perspective = Calib3d.findHomography(perspectiveSourcePoints, perspectiveDestinationPoints)
        inversePerspective = Calib3d.findHomography(perspectiveDestinationPoints, perspectiveSourcePoints)
    }

    inner class FitTarget(initial: RotatedRect, tier: Int) {
        val fittedEllipses = mutableListOf<Pair<RotatedRect, Int>>(Pair(initial, tier))

        val center: Point
            get() {
                var x = 0.0
                var y = 0.0
                for (e in fittedEllipses) {
                    x += e.first.center.x
                    y += e.first.center.y
                }
                return Point(x / fittedEllipses.size, y / fittedEllipses.size)
            }

        val tierWhite: RotatedRect
            get() {
                var w = 0.0
                var h = 0.0
                var angle = 0.0
                for ((e, t) in fittedEllipses) {
                    w += e.size.width * Config.Target.TIER_WHITE / t
                    h += e.size.height * Config.Target.TIER_WHITE / t
                    angle += if (e.angle > 90) e.angle - 180 else e.angle
                }
                w /= fittedEllipses.size
                h /= fittedEllipses.size
                angle /= fittedEllipses.size
                if (angle < 0)
                    angle += 180

                return RotatedRect(center, Size(w, h), angle)
            }

        fun ellipseToPoints(e: RotatedRect): List<Point> {
            val points = Mat()
            Imgproc.boxPoints(e, points)
            return listOf(
                    Point((points.get(0, 0)[0] + points.get(1, 0)[0]) / 2,
                            (points.get(0, 1)[0] + points.get(1, 1)[0]) / 2),
                    Point((points.get(1, 0)[0] + points.get(2, 0)[0]) / 2,
                            (points.get(1, 1)[0] + points.get(2, 1)[0]) / 2),
                    Point((points.get(2, 0)[0] + points.get(3, 0)[0]) / 2,
                            (points.get(2, 1)[0] + points.get(3, 1)[0]) / 2),
                    Point((points.get(3, 0)[0] + points.get(0, 0)[0]) / 2,
                            (points.get(3, 1)[0] + points.get(0, 1)[0]) / 2)
            )
        }


        fun getTransformedTarget(): Mat {
            val perspectiveImg = img.clone()
            Imgproc.warpPerspective(img, perspectiveImg, perspective, Size(Target.TARGET_IMAGE_SIZE, Target.TARGET_IMAGE_SIZE))
            return perspectiveImg
        }

        fun targetWithinImage(): Boolean {
//            val points = Mat()
//            Imgproc.boxPoints(tierWhite, points)

//            for (i in 0..3) {
//                val x = points.get(i, 0)[0]
//                val y = points.get(i, 1)[0]
//                if (x < 0 || x > img.width() || y < 0 || y > img.height())
//                    return false
//            }

            val points = ellipseToPoints(tierWhite)
            for (p in points) {
                if (p.x < 0 || p.x > img.width() || p.y < 0 || p.y > img.height())
                    return false
            }

            return true
        }
//
//        val perspective2: Mat
//            get() {
//                val surf = FeatureDetector.create(FeatureDetector.SURF)
//
//
//                return Mat()
//            }

        var perspectiveSourcePoints: MatOfPoint2f = MatOfPoint2f()
        var perspectiveDestinationPoints: MatOfPoint2f = MatOfPoint2f()

        fun updatePerspectivePoints() {
            val sourcePoints = mutableListOf<Point>()
            val destinationPoints = mutableListOf<Point>()

            for ((e, t) in fittedEllipses) {
                for (i in 0..360 step 5) {
                    val x = e.size.width * Math.cos(Math.toRadians(i - e.angle)) / 2
                    val y = e.size.height * Math.sin(Math.toRadians(i - e.angle)) / 2
                    val a = Math.toRadians(e.angle)
                    sourcePoints.add(
                            Point(
                                    e.center.x + (x * Math.cos(a) - y * Math.sin(a)),
                                    e.center.y + (x * Math.sin(a) + y * Math.cos(a))
                            )
                    )

                    destinationPoints.add(
                            Point(
                                    Config.Target.TARGET_CENTER.x + Config.Target.TARGET_RADIUS * t * Math.cos(Math.toRadians(i.toDouble())) / Config.Target.TIER_WHITE,
                                    Config.Target.TARGET_CENTER.y + Config.Target.TARGET_RADIUS * t * Math.sin(Math.toRadians(i.toDouble())) / Config.Target.TIER_WHITE
                            )
                    )
                }
            }

            sourcePoints.add(center)
            destinationPoints.add(Target.TARGET_CENTER)

            perspectiveSourcePoints = MatOfPoint2f()
            perspectiveSourcePoints.fromArray(*sourcePoints.toTypedArray())
            perspectiveDestinationPoints = MatOfPoint2f()
            perspectiveDestinationPoints.fromArray(*destinationPoints.toTypedArray())

            perspective = Calib3d.findHomography(perspectiveSourcePoints, perspectiveDestinationPoints)
            inversePerspective = Calib3d.findHomography(perspectiveDestinationPoints, perspectiveSourcePoints)
        }

        lateinit var perspective: Mat
        lateinit var inversePerspective: Mat

        val ringList: List<Pair<Double, Scalar>> by lazy {
            val res = mutableListOf<Pair<Double, Scalar>>()

            for (i in 10 downTo 1) {
                res.add(Pair(i / 10.0,
                        (when (i) {
                            9, 10 -> Color.WHITE
                            7, 8 -> Color.BLACK
                            5, 6 -> Color.BLUE
                            3, 4 -> Color.RED
                            else -> Color.YELLOW
                        }).asBGRScalar()))
            }

            res
        }

        fun allRings(): List<Pair<RotatedRect, Scalar>> {
            val outerMost = tierWhite
            return ringList.map {
                Pair(
                        RotatedRect(
                                outerMost.center,
                                Size(outerMost.size.width * it.first, outerMost.size.height * it.first),
                                outerMost.angle
                        ), it.second)
            }
        }

        fun fit(e: RotatedRect, t: Int) {
            for ((ellipse, tier) in fittedEllipses) {
                if (MatHelper.distance(ellipse.center, e.center) > 10)
                    return
//                if (Math.abs(MatHelper.angleDifference(ellipse.angle.toInt(), e.angle.toInt(), 90)) > 10)
//                    return
//                val ellipseE = MatHelper.eccentricity(ellipse)
//                val eE = MatHelper.eccentricity(e)
                if (Math.abs(MatHelper.eccentricity(ellipse) - MatHelper.eccentricity(e)) > 0.3)
                    return
//                val ellipseA = ellipse.size.area() * t * t
//                val eA = e.size.area() * tier * tier
//                val diff = ellipseA - eA
                if (Math.abs(ellipse.size.area() * t * t - e.size.area() * tier * tier) > 30000)
                    return

            }

            fittedEllipses.add(Pair(e, t))
            updatePerspectivePoints()
        }

        fun perspectivePointTransform(p: Point): Point {
            val dst = Mat()
            val src = Mat(1, 1, CvType.CV_32FC2)
            src.put(0, 0, p.x, p.y)
            Core.perspectiveTransform(src, dst, perspective)
            return Point(dst.get(0, 0)[0], dst.get(0, 0)[1])
        }

        fun inversePerspectivePointTransform(p: Point): Point {
            val dst = Mat()
            val src = Mat(1, 1, CvType.CV_32FC2)
            src.put(0, 0, p.x, p.y)
            Core.perspectiveTransform(src, dst, inversePerspective)

            return Point(dst.get(0, 0)[0], dst.get(0, 0)[1])
        }
    }

    fun getTransformedTarget(): Mat {
        val perspectiveImg = img.clone()
        Imgproc.warpPerspective(img, perspectiveImg, perspective, Size(Target.TARGET_IMAGE_SIZE, Target.TARGET_IMAGE_SIZE))
        return perspectiveImg
    }

    fun perspectivePointTransform(p: Point): Point {
        val dst = Mat()
        val src = Mat(1, 1, CvType.CV_32FC2)
        src.put(0, 0, p.x, p.y)
        Core.perspectiveTransform(src, dst, perspective)
        return Point(dst.get(0, 0)[0], dst.get(0, 0)[1])
    }

    fun inversePerspectivePointTransform(p: Point): Point {
        val dst = Mat()
        val src = Mat(1, 1, CvType.CV_32FC2)
        src.put(0, 0, p.x, p.y)
        Core.perspectiveTransform(src, dst, inversePerspective)

        return Point(dst.get(0, 0)[0], dst.get(0, 0)[1])
    }


    private fun detectTarget() {
        val potentialTargets = mutableListOf<FitTarget>()


        val gold = detectCircles(closest, Color.YELLOW, Config.Target.TIER_GOLD * Config.Target.TIER_GOLD * 2000)
//        drawEllipses(gold, Colors.COLOR_YELLOW)
        for (e in gold) {
            potentialTargets.add(FitTarget(e, Config.Target.TIER_GOLD))
        }

        val red = detectCircles(closest, Color.RED, Config.Target.TIER_RED * Config.Target.TIER_RED * 2000)
//        drawEllipses(red, Colors.COLOR_RED)
        for (e in red) {
            for (t in potentialTargets) {
                t.fit(e, Config.Target.TIER_RED)
            }
        }
        for (e in red) {
            potentialTargets.add(FitTarget(e, Config.Target.TIER_RED))
        }

        val blue = detectCircles(closest, Color.BLUE, Config.Target.TIER_BLUE * Config.Target.TIER_BLUE * 2000)
//        drawEllipses(blue, Colors.COLOR_BLUE)
        for (e in blue) {
            for (t in potentialTargets) {
                t.fit(e, Config.Target.TIER_BLUE)
            }
        }
        val imgCenter = Point(img.size().width / 2, img.size().height / 2)
        val greaterThan = object : Comparator<FitTarget> {
            override fun compare(x: FitTarget, y: FitTarget): Int {
                if (x.fittedEllipses.size == y.fittedEllipses.size) {
                    val d1 = MatHelper.distance(x.center, imgCenter)
                    val d2 = MatHelper.distance(y.center, imgCenter)
                    return if (d1 > d2) -1 else (if (d1 < d2) 1 else 0)
                }
                return x.fittedEllipses.size - y.fittedEllipses.size
            }
        }

//        for (t in potentialTargets) {
//            if (t.fittedEllipses.size == 3) {
//                val points = Mat()
//                Imgproc.boxPoints(t.tierWhite, points)
////                println(points.dump())
//                drawEllipses(t.allRings())
//            }
//        }

        val fitted = potentialTargets.filter { it.targetWithinImage() }.maxWith(greaterThan)!!
        bestFittedTarget = fitted

        val perspectiveImg = img.clone()
        Imgproc.warpPerspective(img, perspectiveImg, fitted.perspective, Size(Config.Target.TARGET_IMAGE_SIZE, Config.Target.TARGET_IMAGE_SIZE))
//        logger.log("perspective", perspectiveImg)
//        logger.log("closest", closest)

//
//        val rings = fitted!!.allRings()
//        drawEllipses(rings)
//        for (e in blue) {
//            potentialTargets.add(FitTarget(e, TIER_BLUE))
//        }


//        val black = detectCircles(closest, Colors.COLOR_BLACK)
//        drawEllipses(black, Colors.COLOR_BLACK)


    }

    private fun detectCircles(closest: Mat, color: Color, minArea: Int = 2000): List<RotatedRect> {
        val dilationSize = 10.0

        val singleColor = MatHelper.extractColor(closest, color)
        val borders = Mat()

        Imgproc.cvtColor(singleColor, borders, Imgproc.COLOR_BGR2GRAY)
        val t = 20.0
        Imgproc.Canny(borders, borders, t, t * 3)
        val element = Imgproc.getStructuringElement(Imgproc.MORPH_CROSS, Size(dilationSize, dilationSize))
        Imgproc.dilate(borders, borders, element)

        val contours = mutableListOf<MatOfPoint>()
        val hierarchy = Mat()
        Imgproc.findContours(borders, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE)

        val ellipses = mutableListOf<RotatedRect>()

        for ((i, c) in contours.withIndex()) {


            val hull = MatOfInt()
            Imgproc.convexHull(c, hull)

            val hullPoints = MatHelper.convertIndexesToPoints(c, hull)

            val area = Imgproc.contourArea(hullPoints)

            if (area < minArea)
                continue

            val cnt = MatOfPoint2f()
            hullPoints.convertTo(cnt, CvType.CV_32F);

            val ellipse = Imgproc.fitEllipse(cnt)

            ellipse.size = Size(ellipse.size.width - dilationSize, ellipse.size.height - dilationSize)
            ellipses.add(ellipse)

//            val cntOrg = MatOfPoint2f()
//            c.convertTo(cntOrg, CvType.CV_32F);
//            val originalEllipse = Imgproc.fitEllipse(cntOrg)
//            originalEllipse.size = Size(originalEllipse.size.width - dilationSize, originalEllipse.size.height - dilationSize)
//            ellipses.add(originalEllipse)
        }
        return ellipses
    }

    fun pointsAroundTheCircle(center: Point, radius: Double, count: Int): List<Point> {
        val points = mutableListOf<Point>()
        for (i in 0..360 step 360 / count) {
            points.add(
                    Point(
                            center.x + radius * Math.cos(Math.toRadians(i.toDouble())),
                            center.y + radius * Math.sin(Math.toRadians(i.toDouble()))
                    )
            )
        }
        return points
    }

    fun isPointInTier(p: Point, t: Int): Boolean {
        val d = LineHelper.distanceBetweenPoints(p, Config.Target.TARGET_CENTER)
        val lb = (t - 1) * (Config.Target.TARGET_RADIUS / Config.Target.TIER_WHITE)
        val ub = (t) * (Config.Target.TARGET_RADIUS / Config.Target.TIER_WHITE)
        return d in lb..ub
    }

    fun pointScore(p: Point): Int {
        val d = Math.max(LineHelper.distanceBetweenPoints(p, Config.Target.TARGET_CENTER) - arrowDiameterPixels, 0.0)
        val relativeD = d / Config.Target.TARGET_RADIUS
        for ((s, min, max) in targetScores) {
            if (relativeD > min && relativeD <= max) {
                return s
            }
        }

        return 0
    }

    fun pointToRealMetrics(p: Point): Point {
        val x = (p.x - Config.Target.TARGET_CENTER.x) * targetDiameter / (2 * Config.Target.TARGET_RADIUS)
        val y = (p.y - Config.Target.TARGET_CENTER.y) * targetDiameter / (2 * Config.Target.TARGET_RADIUS)

        return Point(x, y)
    }


    fun pointOnTarget(p: Point): Boolean {
        val d = LineHelper.distanceBetweenPoints(p, Config.Target.TARGET_CENTER)
        return d <= Config.Target.TARGET_RADIUS
    }

    fun isLneTangentOfTargetCircles(p1: Point, p2: Point): Boolean {
//        return false
        //TODO: Improve function
        val d1 = LineHelper.distanceBetweenPoints(p1, Config.Target.TARGET_CENTER)
        val d2 = LineHelper.distanceBetweenPoints(p2, Config.Target.TARGET_CENTER)

        if (Math.abs(d1 - d2) < 5.0) {
            val r = (d1 + d2) / 2
            val midPoint = Point((p1.x + p2.x) / 2, (p1.y + p2.y) / 2)
            val d = LineHelper.distanceBetweenPoints(midPoint, Config.Target.TARGET_CENTER)
            return Math.abs(r - d) < 5.0
        }
        return false
    }
}