package com.orengy.archinator.common

import com.orengy.archinator.common.logger.AbstractLogger
import com.orengy.archinator.common.logger.EmptyLogger
import com.orengy.archinator.common.model.ScoredArrow
import org.opencv.core.*
import org.opencv.imgcodecs.Imgcodecs
import org.opencv.imgproc.Imgproc
import org.opencv.core.CvType
import org.opencv.core.Mat
import kotlin.math.min
import java.io.File


class Arrows(
        val img: Mat,
        val referenceTarget: TargetDetector? = null,
        val targetScores: List<Triple<Int, Double, Double>> = listOf(
                Triple(10, 0.0, 0.10),
                Triple(9, 0.10, 0.20),
                Triple(8, 0.20, 0.30),
                Triple(7, 0.30, 0.40),
                Triple(6, 0.40, 0.50),
                Triple(5, 0.50, 0.60),
                Triple(4, 0.60, 0.70),
                Triple(3, 0.70, 0.80),
                Triple(2, 0.80, 0.90),
                Triple(1, 0.90, 1.0),
                Triple(0, 1.0, 1000.0)
        ),
        val targetDiameter: Double = 0.6,
        val arrowDiameterMm: Double = 6.0,
        val arrowShaftColor: Color = Color.BLACK,
        val logger: AbstractLogger = EmptyLogger()) {

    val arrowDiameterPixels: Double = arrowDiameterMm * Config.Target.TARGET_RADIUS / (1000.0 * targetDiameter)
    val target = TargetDetector(img, targetScores, targetDiameter, arrowDiameterMm, referenceTarget, logger)
    val targetImg: Mat
    val targetImgClosest: Mat

    init {
        targetImg = target.getTransformedTarget()
        targetImgClosest = MatHelper.closestColors(targetImg)
    }


    inner class MergedLine(x1: Double, y1: Double, x2: Double, y2: Double) {
        val points = mutableListOf<Double>(x1, y1, x2, y2)

        fun getLine(): Pair<Point, Point> {
            val matPoints = Mat(1, points.size / 2, CvType.CV_32FC2)

            matPoints.put(0, 0, *points.toDoubleArray())
            val dst = Mat()
            Imgproc.fitLine(matPoints, dst, Imgproc.CV_DIST_L12, 0.0, 0.01, 0.01)

            val d = Point(dst.get(0, 0)[0], dst.get(1, 0)[0])
            val p = Point(dst.get(2, 0)[0], dst.get(3, 0)[0])

            return Pair(d, p)
        }

        fun getLinePoints(): Pair<Point, Point> {
            val (n, a) = getLine()
            return getLinePoints(n, a)
        }

        fun getLinePoints(n: Point, a: Point): Pair<Point, Point> {

            val distantPointX = a.x + 10000 * n.x
            val distantPointY = a.y + 10000 * n.y

            val l = (0..(points.size - 1) step 2).map {
                Pair(Point(points[it], points[it + 1]),
                        Math.pow(distantPointX - points[it], 2.0) +
                                Math.pow(distantPointY - points[it + 1], 2.0)
                )
            }

            val p1 = l.minBy { it.second }
            val p2 = l.maxBy { it.second }

//            return Pair(p1!!.first, p2!!.first)
            return Pair(LineHelper.projectOnLine(p1!!.first, n, a), LineHelper.projectOnLine(p2!!.first, n, a))
        }

        fun addLine(x1: Double, y1: Double, x2: Double, y2: Double): Boolean {
            val (n, a) = getLine()
//            val (p1, p2) = getLinePoints(n, a)

            val d1 = LineHelper.perpendicularDistanceToLine(Point(x1, y1), n, a)
            val d2 = LineHelper.perpendicularDistanceToLine(Point(x2, y2), n, a)

            val maxDistanceToLine = 5.0
            val maxDistanceAlongLine = 100.0

            if (d1 < maxDistanceToLine && d2 < maxDistanceToLine) {

                val dl1 = distanceAlongTheLineToLine(Point(x1, y1), n, a)
                val dl2 = distanceAlongTheLineToLine(Point(x2, y2), n, a)

                if (dl1 < maxDistanceAlongLine || dl2 < maxDistanceAlongLine) {
                    points.add(x1)
                    points.add(y1)
                    points.add(x2)
                    points.add(y2)
                    return true
                }
            }
            return false
        }

        fun distanceAlongTheLineToLine(p: Point, n: Point, a: Point): Double {
            val (p1, p2) = getLinePoints(n, a)
            val projectedPoint = LineHelper.projectOnLine(p, n, a)

            val distantPoint = Point(a.x + 10000 * n.x, a.y + 10000 * n.y)
            val d = LineHelper.distanceBetweenPoints(distantPoint, projectedPoint)
            val d1 = LineHelper.distanceBetweenPoints(distantPoint, p1)
            val d2 = LineHelper.distanceBetweenPoints(distantPoint, p2)

            val dMin = Math.min(d1, d2)
            val dMax = Math.max(d1, d2)

            if (d in dMin..dMax) {
                return 0.0
            } else if (d < dMin) {
                return dMin - d
            } else {
                return d - dMax
            }
        }

    }

    var num = 0

    inner class Arrow(val ml1: MergedLine, val ml2: MergedLine) {

        var isValidArrow: Boolean
            private set
        var arrowTipPoint: Point
            private set

        init {
            val (p1, p2) = ml1.getLinePoints()
            val (p3, p4) = ml2.getLinePoints()

            val (n1, a1) = ml1.getLine()
            val (n2, a2) = ml2.getLine()
            val (n, a) = LineHelper.centerLine(n1, a1, n2, a2)

            val distantPoint = Point(n.x * 10000 + a.x, n.y * 10000 + a.y)

            val points = listOf(p1, p2, p3, p4)
            val lp1 = LineHelper.projectOnLine(points.maxBy { LineHelper.distanceBetweenPoints(distantPoint, LineHelper.projectOnLine(it, n, a)) }!!, n, a)
            val lp2 = LineHelper.projectOnLine(points.minBy { LineHelper.distanceBetweenPoints(distantPoint, LineHelper.projectOnLine(it, n, a)) }!!, n, a)
            val tlp1 = target.inversePerspectivePointTransform(lp1)
            val tlp2 = target.inversePerspectivePointTransform(lp2)

            val tipPoint = if (tlp1.y < tlp2.y) lp1 else lp2
            val nockPoint = if (tlp1.y < tlp2.y) lp2 else lp1

            val tip1 = LineHelper.projectOnLine(tipPoint, n1, a1)
            val tip2 = LineHelper.projectOnLine(tipPoint, n2, a2)
            val nock1 = LineHelper.projectOnLine(nockPoint, n1, a1)
            val nock2 = LineHelper.projectOnLine(nockPoint, n2, a2)
            val res = Point(tip2.x - tip1.x - nock2.x + nock1.x, tip2.y - tip1.y - nock2.y + nock1.y)
            val diff = LineHelper.vectorLength(res)
            if (diff >= 20 || diff <= 0) {
                isValidArrow = false
                arrowTipPoint = tipPoint
            } else {


                val dTip = LineHelper.distanceBetweenPoints(tipPoint, distantPoint)
                val dNock = LineHelper.distanceBetweenPoints(nockPoint, distantPoint)

                val steps = Math.round(Math.abs(dTip - dNock)).toInt()
                val direction = if (dTip < dNock) -1 else 1

                var properArrowStart = -1
                var currentLen = 0
                val trg = targetImg.clone()
                val trg2 = targetImg.clone()

                val minProperLength = 50
                for (s in 0..steps) {
                    val x = Math.round(tipPoint.x + direction * s * n.x).toInt()
                    val y = Math.round(tipPoint.y + direction * s * n.y).toInt()
                    Imgproc.circle(trg, Point(x.toDouble(), y.toDouble()), 1, Scalar(0.0, 0.0, 255.0), 1)


                    val color = try {
                        targetImgClosest.get(y, x) ?: break
                    } catch (ex: Throwable) {
                        println("-------------")
                        println(x)
                        println(y)
                        println("-------------")
                        logger.log("what_the", trg)
                        break
                    }

                    if (arrowShaftColor.isTheSameColor(Color(color[2].toInt(), color[1].toInt(), color[0].toInt()))) {
                        if (properArrowStart == -1) {
                            properArrowStart = s
                        }
                        if (!target.isPointInTier(Point(x.toDouble(), y.toDouble()), arrowShaftColor.colorOfTier())) {
                            currentLen++
                        }

                    } else {
                        properArrowStart = -1
                        currentLen = 0
                    }

                    if (currentLen >= minProperLength) {
                        break
                    }
                }
                Imgproc.line(trg, p1, p2, Scalar(255.0, 0.0, 0.0), 2)
                Imgproc.line(trg, p3, p4, Scalar(255.0, 0.0, 0.0), 2)

                isValidArrow = currentLen >= minProperLength
                arrowTipPoint = Point(
                        tipPoint.x + direction * (properArrowStart + arrowDiameterPixels / 2) * n.x,
                        tipPoint.y + direction * (properArrowStart + arrowDiameterPixels / 2) * n.y)
                Imgproc.circle(trg, arrowTipPoint, 2, Scalar(0.0, 255.0, 0.0), 2)

                if (isValidArrow) {
//                    logger.log("potential_arrow_$num", trg)
//                    Imgcodecs.imwrite("C:\\Users\\Adomas\\Documents\\Projects\\archinator\\arrows\\$num-$name", trg)


                    val tip1 = LineHelper.projectOnLine(arrowTipPoint, n1, a1)
                    val tip2 = LineHelper.projectOnLine(arrowTipPoint, n2, a2)

                    val d1 = LineHelper.distanceBetweenPoints(tip1, tip2)
                    val d2 = LineHelper.distanceBetweenPoints(nock1, nock2)
                    val w = Math.max(d1, d2) + 30
                    val h = LineHelper.distanceBetweenPoints(nockPoint, arrowTipPoint) + 30
                    val c = LineHelper.middlePoint(nockPoint, arrowTipPoint)
//                    val arrowRect = RotatedRect(
//                            LineHelper.middlePoint(nockPoint, arrowTipPoint),
//                            Size(h, w),
//                            Math.toDegrees(Math.atan2(n.y, n.x))
//                    )

//                    val arrowPoints: Array<Point?> = arrayOf(null, null, null, null)
//                    arrowRect.points(arrowPoints)
//                    for (i in 0 until arrowPoints.size) {
//                        Imgproc.line(trg2, arrowPoints[i], arrowPoints[(i + 1) % 4], Scalar(0.0, 255.0, 0.0), 2)
//                    }

//                    val rot = Imgproc.getRotationMatrix2D(c, Math.toDegrees(Math.atan2(arrowTipPoint.y - nockPoint.y, arrowTipPoint.x - nockPoint.x)), 1.0)
//                    val rotatedMat = Mat()
//                    val croppedMat = Mat()
//                    Imgproc.warpAffine(trg2, rotatedMat, rot, trg2.size(), Imgproc.INTER_CUBIC);
//                    Imgproc.getRectSubPix(rotatedMat, Size(h, w), c, croppedMat);

//                    val validFile = File("C:\\Users\\Adomas\\Documents\\Projects\\archinator\\bounded-arrows-correct\\$num-$name")
//                    if (false &&  !validFile.exists()) {
//                        isValidArrow = false
//                    }

//                    Imgcodecs.imwrite("C:\\Users\\Adomas\\Documents\\Archinator\\bounded-arrows\\$num-$name", croppedMat)
                }
            }
//            if (isValidArrow)
//                Imgproc.circle(targetImg, arrowTipPoint, 2, Scalar(255.0, 0.0, 0.0), 2)
            num++
        }

//        fun isValid(): Boolean {
//
//        }
    }

    fun findArrows(): List<ScoredArrow> {
//        return listOf()
        val file = targetImg.clone()
        Imgproc.medianBlur(file, file, 5)

        Imgproc.cvtColor(file, file, Imgproc.COLOR_BGR2GRAY)
        Imgproc.blur(file, file, Size(4.0, 4.0))
        val t = 20.0
        Imgproc.Canny(file, file, t, t * 3)
        val element = Imgproc.getStructuringElement(Imgproc.MORPH_CROSS, Size(3.0, 3.0))
        Imgproc.dilate(file, file, element)

        val lines = Mat()

        Imgproc.HoughLinesP(file, lines, 1.0, Math.PI / 180, 100, 10.0, 5.0)
        Imgproc.cvtColor(file, file, Imgproc.COLOR_GRAY2BGR)
        val ll = img.clone()
        val mergedLines = mutableListOf<MergedLine>()

        for (i in 0 until lines.rows()) {
            val vec = lines.get(i, 0)
            val x1 = vec[0]
            val y1 = vec[1]
            val x2 = vec[2]
            val y2 = vec[3]

            if (target.isLneTangentOfTargetCircles(Point(x1, y1), Point(x2, y2))) {
                continue
            }

            var foundFit = false
            for (ml in mergedLines) {
                if (ml.addLine(x1, y1, x2, y2)) {
                    foundFit = true
                    break
                }
            }
            if (!foundFit) {
                mergedLines.add(MergedLine(x1, y1, x2, y2))
            }

//            Imgproc.line(ll, Point(x1, y1), Point(x2, y2), Scalar(255.0 * (i % 2), 255.0 * ((i + 1) % 2), 127.5 * (i % 3)), 2)

        }

        mergedLines.sortBy {
            val (p1, p2) = it.getLinePoints()
            Math.min(p1.y, p2.y)
        }

        val removeLines = mutableListOf<MergedLine>()

        for ((i, ml) in mergedLines.withIndex()) {
            val (p1, p2) = ml.getLinePoints()

            for (j in 0 until i) {
                if (!removeLines.contains(mergedLines[j]) && mergedLines[j].addLine(p1.x, p1.y, p2.x, p2.y)) {
                    removeLines.add(ml)
                    break
                }
            }
        }

        mergedLines.removeAll(removeLines)

//        for ((i, ml) in mergedLines.withIndex()) {
//            val (p1, p2) = ml.getLinePoints()
//            Imgproc.line(file, p1, p2, Scalar(255.0 * (i % 2), 255.0 * ((i + 1) % 2), 127.5 * (i % 3)), 2)
//        }
        val potentialArrows = mutableListOf<Arrow>()
        val minAngleToConsiderParallel = 5.0
        val maxArrowWidth = 30.0
        for (i in 0 until mergedLines.size) {
            for (j in i + 1 until mergedLines.size) {
                val (n1, a1) = mergedLines[i].getLine()
                val (n2, _) = mergedLines[j].getLine()
                val (p1, p2) = mergedLines[i].getLinePoints()
                val (p3, p4) = mergedLines[j].getLinePoints()


                val diff = Math.toDegrees(Math.acos(n1.x * n2.x + n1.y * n2.y))
                val d1 = LineHelper.perpendicularDistanceToLine(p3, n1, a1)
                val d2 = LineHelper.perpendicularDistanceToLine(p4, n1, a1)



                if (diff < minAngleToConsiderParallel
                        && d1 < maxArrowWidth
                        && d2 < maxArrowWidth
                        && LineHelper.doLinesOverlap(n1, a1, p1, p2, p3, p4)
                        && (target.pointOnTarget(p1)
                                || target.pointOnTarget(p2))
                        && (target.pointOnTarget(p3)
                                || target.pointOnTarget(p4))
                        && !target.isLneTangentOfTargetCircles(p1, p2)
                        && !target.isLneTangentOfTargetCircles(p3, p4)
                ) {
                    potentialArrows.add(Arrow(mergedLines[i], mergedLines[j]))
                }
            }
        }

//        val closest = MatHelper.closestColors(targetImg)


//        for ((i, arrow) in potentialArrows.withIndex()) {
//
//            val (p1, p2) = arrow.ml1.getLinePoints()
//            val (p3, p4) = arrow.ml2.getLinePoints()
////            Imgproc.line(closest, p1, p2, Scalar(255.0 * (i % 2), 255.0 * ((i + 1) % 2), 127.5 * (i % 3)), 2)
////            Imgproc.line(closest, p3, p4, Scalar(255.0 * (i % 2), 255.0 * ((i + 1) % 2), 127.5 * (i % 3)), 2)
//
//            Imgproc.line(ll, target.inversePerspectivePointTransform(p1), target.inversePerspectivePointTransform(p2), Scalar(255.0 * (i % 2), 255.0 * ((i + 1) % 2), 127.5 * (i % 3)), 2)
//            Imgproc.line(ll, target.inversePerspectivePointTransform(p3), target.inversePerspectivePointTransform(p4), Scalar(255.0 * (i % 2), 255.0 * ((i + 1) % 2), 127.5 * (i % 3)), 2)
//
//            val (n1, a1) = arrow.ml1.getLine()
//            val (n2, a2) = arrow.ml2.getLine()
//            val (n, a) = LineHelper.centerLine(n1, a1, n2, a2)
//            val (cp1, cp2) = LineHelper.infiniteLineToPoints(n, a)
//
////            Imgproc.line(closest, cp1, cp2, Scalar(255.0 * (i % 2), 255.0 * ((i + 1) % 2), 127.5 * (i % 3)), 2)
//        }

//        val mat = Mat(2, 2, CvType.CV_32FC1)
//        mat.put()


//        Imgcodecs.imwrite("C:\\Users\\Adomas\\Documents\\Projects\\archinator\\arrows\\short-$name", ll)
//        Imgcodecs.imwrite("C:\\Users\\Adomas\\Documents\\Projects\\archinator\\arrows\\$name", closest)
//        Imgcodecs.imwrite("C:\\Users\\Adomas\\Documents\\Projects\\archinator\\arrows\\target-$name", targetImg)

        return potentialArrows.filter { it.isValidArrow }.map { ScoredArrow(it.arrowTipPoint, target.pointScore(it.arrowTipPoint)) }
    }
}