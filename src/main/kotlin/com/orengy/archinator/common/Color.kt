package com.orengy.archinator.common

import org.opencv.core.Scalar

class Color(val r: Int, val g: Int, val b: Int) {

    constructor(rgb: Int) : this(rgb shr 16 and 0xFF, rgb shr 8 and 0xFF, rgb and 0xFF)

    companion object {
        val WHITE = Color(255, 255, 255)
        val GREY = Color(244, 67, 54)
        val RED = Color(244, 67, 54)
        val BLACK = Color(0, 0, 0)
        val PINK = Color(233, 30, 99)
        val PURPLE = Color(156, 39, 176)
        val BLUE = Color(33, 150, 243)
        val GREEN = Color(76, 175, 80)
        val YELLOW = Color(255, 235, 59)
        val ORANGE = Color(255, 152, 0)


        val targetColors = listOf(
                WHITE,
//            GREY,
                RED,
                BLACK,
//            PINK,
                PURPLE,
                BLUE,
                GREEN,
                YELLOW
//            ORANGE,

        )
//        val fullColors = listOf<LAB>(
//                COLOR_WHITE,
////            COLOR_GREY,
//                COLOR_RED,
//                COLOR_BLACK,
////            COLOR_PINK,
//                COLOR_PURPLE,
//                COLOR_BLUE,
//                COLOR_GREEN,
//                COLOR_YELLOW
////            COLOR_ORANGE,
//
//        )

        fun tierColor(tier: Int): Color {
            return when (tier) {
                Config.Target.TIER_BLACK -> BLACK
                Config.Target.TIER_WHITE -> WHITE
                Config.Target.TIER_RED -> RED
                Config.Target.TIER_GOLD -> YELLOW
                Config.Target.TIER_BLUE -> BLUE
                else -> GREEN
            }
        }
    }

    fun asBGRScalar(): Scalar {
        return Scalar(b.toDouble(), g.toDouble(), r.toDouble())
    }

    fun isTheSameColor(c: Color): Boolean {
        return c.r == r && c.g == g && c.b == b
    }

    fun colorOfTier(): Int {
        return when {
            isTheSameColor(BLACK) -> Config.Target.TIER_BLACK
            isTheSameColor(BLUE) -> Config.Target.TIER_BLUE
            isTheSameColor(WHITE) -> Config.Target.TIER_WHITE
            isTheSameColor(YELLOW) -> Config.Target.TIER_GOLD
            isTheSameColor(RED) -> Config.Target.TIER_RED
            else -> 0
        }
    }
}