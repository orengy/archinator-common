package com.orengy.archinator.common.logger

import org.opencv.core.Mat

abstract class AbstractLogger {
    abstract fun log(tag: String, img: Mat)
}