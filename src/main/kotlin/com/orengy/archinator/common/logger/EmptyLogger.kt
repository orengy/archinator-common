package com.orengy.archinator.common.logger

import org.opencv.core.Mat

class EmptyLogger : AbstractLogger() {
    override fun log(tag: String, img: Mat){}
}