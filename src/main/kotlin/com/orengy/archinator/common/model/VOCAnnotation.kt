package com.orengy.archinator.common.model

/**
 * Created by Adomas on 2018-01-06.
 */
class VOCAnnotation {
    lateinit var filename: String
    lateinit var path: String
    lateinit var size: Size
    lateinit var `object`: List<AnnotatedObject>

    inner class AnnotatedObject {
        lateinit var name: String
        lateinit var bndbox: BoundingBox

        inner class BoundingBox {
            var xmin: Int = 0
            var ymin: Int = 0
            var xmax: Int = 0
            var ymax: Int = 0
        }
    }

    inner class Size {
        var width: Int = 0
        var height: Int = 0
        var depth: Int = 0
    }
}