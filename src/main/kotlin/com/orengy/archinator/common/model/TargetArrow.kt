package com.orengy.archinator.common.model

/**
 * Created by Adomas on 2017-11-21.
 */
class TargetArrow {
    val score: Int = 0
    val lineCutter: Boolean = false
}