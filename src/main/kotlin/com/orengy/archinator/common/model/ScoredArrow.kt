package com.orengy.archinator.common.model

import org.opencv.core.Point

/**
 * Created by Adomas on 2017-12-28.
 */
data class ScoredArrow(
        val tipPoint: Point,
        val score: Int
)