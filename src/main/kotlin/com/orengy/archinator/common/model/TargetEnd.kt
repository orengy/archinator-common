package com.orengy.archinator.common.model

/**
 * Created by Adomas on 2017-11-21.
 */
class TargetEnd {
    lateinit var file: String
    lateinit var arrows: List<TargetArrow>
}