package com.orengy.archinator.common

import com.orengy.archinator.common.logger.AbstractLogger
import com.orengy.archinator.common.logger.EmptyLogger
import org.opencv.calib3d.Calib3d
import org.opencv.core.*
import org.opencv.features2d.FeatureDetector
import org.opencv.imgcodecs.Imgcodecs
import org.opencv.imgproc.Imgproc
import org.opencv.features2d.Features2d
import org.opencv.core.MatOfPoint2f
import org.opencv.core.CvType
import org.opencv.core.Mat


class ReferenceTarget(val img: Mat, val logger: AbstractLogger = EmptyLogger()) {

    lateinit var bestFittedTarget: FitTarget

    init {
        detectTarget()
    }

    inner class FitTarget(initial: RotatedRect, tier: Int) {
        val fittedEllipses = mutableListOf<Pair<RotatedRect, Int>>(Pair(initial, tier))

        val center: Point
            get() {
                var x = 0.0
                var y = 0.0
                for (e in fittedEllipses) {
                    x += e.first.center.x
                    y += e.first.center.y
                }
                return Point(x / fittedEllipses.size, y / fittedEllipses.size)
            }

        val tierWhite: RotatedRect
            get() {
                var w = 0.0
                var h = 0.0
                var angle = 0.0
                for ((e, t) in fittedEllipses) {
                    w += e.size.width * Config.Target.TIER_WHITE / t
                    h += e.size.height * Config.Target.TIER_WHITE / t
                    angle += if (e.angle > 90) e.angle - 180 else e.angle
                }
                w /= fittedEllipses.size
                h /= fittedEllipses.size
                angle /= fittedEllipses.size
                if (angle < 0)
                    angle += 180

                return RotatedRect(center, Size(w, h), angle)
            }

        fun ellipseToPoints(e: RotatedRect): List<Point> {
            val points = Mat()
            Imgproc.boxPoints(e, points)
            return listOf(
                    Point((points.get(0, 0)[0] + points.get(1, 0)[0]) / 2,
                            (points.get(0, 1)[0] + points.get(1, 1)[0]) / 2),
                    Point((points.get(1, 0)[0] + points.get(2, 0)[0]) / 2,
                            (points.get(1, 1)[0] + points.get(2, 1)[0]) / 2),
                    Point((points.get(2, 0)[0] + points.get(3, 0)[0]) / 2,
                            (points.get(2, 1)[0] + points.get(3, 1)[0]) / 2),
                    Point((points.get(3, 0)[0] + points.get(0, 0)[0]) / 2,
                            (points.get(3, 1)[0] + points.get(0, 1)[0]) / 2)
            )
        }

        fun targetWithinImage(): Boolean {
//            val points = Mat()
//            Imgproc.boxPoints(tierWhite, points)

//            for (i in 0..3) {
//                val x = points.get(i, 0)[0]
//                val y = points.get(i, 1)[0]
//                if (x < 0 || x > img.width() || y < 0 || y > img.height())
//                    return false
//            }

            val points = ellipseToPoints(tierWhite)
            for (p in points) {
                if (p.x < 0 || p.x > img.width() || p.y < 0 || p.y > img.height())
                    return false
            }

            return true
        }
//
//        val perspective2: Mat
//            get() {
//                val surf = FeatureDetector.create(FeatureDetector.SURF)
//
//
//                return Mat()
//            }

        var perspectiveSourcePoints: MatOfPoint2f = MatOfPoint2f()
        var perspectiveDestinationPoints: MatOfPoint2f = MatOfPoint2f()

        fun updatePerspectivePoints() {
            val sourcePoints = mutableListOf<Point>()
            val destinationPoints = mutableListOf<Point>()

            for ((e, t) in fittedEllipses) {
                for (i in 0..360 step 5) {
                    val x = e.size.width * Math.cos(Math.toRadians(i - e.angle)) / 2
                    val y = e.size.height * Math.sin(Math.toRadians(i - e.angle)) / 2
                    val a = Math.toRadians(e.angle)
                    sourcePoints.add(
                            Point(
                                    e.center.x + (x * Math.cos(a) - y * Math.sin(a)),
                                    e.center.y + (x * Math.sin(a) + y * Math.cos(a))
                            )
                    )

                    destinationPoints.add(
                            Point(
                                    Config.Target.TARGET_PADDING + Config.Target.TARGET_RADIUS + Config.Target.TARGET_RADIUS * t * Math.cos(Math.toRadians(i.toDouble())) / Config.Target.TIER_WHITE,
                                    Config.Target.TARGET_PADDING + Config.Target.TARGET_RADIUS + Config.Target.TARGET_RADIUS * t * Math.sin(Math.toRadians(i.toDouble())) / Config.Target.TIER_WHITE
                            )
                    )
                }
            }

            sourcePoints.add(center)
            destinationPoints.add(Point(Config.Target.TARGET_RADIUS, Config.Target.TARGET_RADIUS))

            perspectiveSourcePoints = MatOfPoint2f()
            perspectiveSourcePoints.fromArray(*sourcePoints.toTypedArray())
            perspectiveDestinationPoints = MatOfPoint2f()
            perspectiveDestinationPoints.fromArray(*destinationPoints.toTypedArray())

            perspective = Calib3d.findHomography(perspectiveSourcePoints, perspectiveDestinationPoints)
            inversePerspective = Calib3d.findHomography(perspectiveDestinationPoints, perspectiveSourcePoints)
        }

        lateinit var perspective: Mat
        lateinit var inversePerspective: Mat

        val ringList: List<Pair<Double, Scalar>> by lazy {
            val res = mutableListOf<Pair<Double, Scalar>>()

            for (i in 10 downTo 1) {
                res.add(Pair(i / 10.0,
                        (when (i) {
                            9, 10 -> Color.WHITE
                            7, 8 -> Color.BLACK
                            5, 6 -> Color.BLUE
                            3, 4 -> Color.RED
                            else -> Color.YELLOW
                        }).asBGRScalar()))
            }

            res
        }

        fun allRings(): List<Pair<RotatedRect, Scalar>> {
            val outerMost = tierWhite
            return ringList.map {
                Pair(
                        RotatedRect(
                                outerMost.center,
                                Size(outerMost.size.width * it.first, outerMost.size.height * it.first),
                                outerMost.angle
                        ), it.second)
            }
        }

        fun fit(e: RotatedRect, t: Int) {
            for ((ellipse, tier) in fittedEllipses) {
                if (MatHelper.distance(ellipse.center, e.center) > 10)
                    return
//                if (Math.abs(MatHelper.angleDifference(ellipse.angle.toInt(), e.angle.toInt(), 90)) > 10)
//                    return
//                val ellipseE = MatHelper.eccentricity(ellipse)
//                val eE = MatHelper.eccentricity(e)
                if (Math.abs(MatHelper.eccentricity(ellipse) - MatHelper.eccentricity(e)) > 0.3)
                    return
//                val ellipseA = ellipse.size.area() * t * t
//                val eA = e.size.area() * tier * tier
//                val diff = ellipseA - eA
                if (Math.abs(ellipse.size.area() * t * t - e.size.area() * tier * tier) > 30000)
                    return

            }

            fittedEllipses.add(Pair(e, t))
            updatePerspectivePoints()
        }
    }

    fun getTransformedTarget(): Mat {
        val perspectiveImg = img.clone()
        Imgproc.warpPerspective(img, perspectiveImg, bestFittedTarget.perspective, Size(Target.TARGET_IMAGE_SIZE, Target.TARGET_IMAGE_SIZE))
        return perspectiveImg
    }

    fun perspectivePointTransform(p: Point): Point {
        val dst = Mat()
        val src = Mat(1, 1, CvType.CV_32FC2)
        src.put(0, 0, p.x, p.y)
        Core.perspectiveTransform(src, dst, bestFittedTarget.perspective)
        return Point(dst.get(0, 0)[0], dst.get(0, 0)[1])
    }

    fun inversePerspectivePointTransform(p: Point): Point {
        val dst = Mat()
        val src = Mat(1, 1, CvType.CV_32FC2)
        src.put(0, 0, p.x, p.y)
        Core.perspectiveTransform(src, dst, bestFittedTarget.inversePerspective)

        return Point(dst.get(0, 0)[0], dst.get(0, 0)[1])
    }


    private fun detectTarget() {
        val potentialTargets = mutableListOf<FitTarget>()

        val closest = MatHelper.closestColors(img)

        val gold = detectCircles(closest, Color.YELLOW, Config.Target.TIER_GOLD * Config.Target.TIER_GOLD * 2000)
//        drawEllipses(gold, Colors.COLOR_YELLOW)
        for (e in gold) {
            potentialTargets.add(FitTarget(e, Config.Target.TIER_GOLD))
        }

        val red = detectCircles(closest, Color.RED, Config.Target.TIER_RED * Config.Target.TIER_RED * 2000)
//        drawEllipses(red, Colors.COLOR_RED)
        for (e in red) {
            for (t in potentialTargets) {
                t.fit(e, Config.Target.TIER_RED)
            }
        }
        for (e in red) {
            potentialTargets.add(FitTarget(e, Config.Target.TIER_RED))
        }

        val blue = detectCircles(closest, Color.BLUE, Config.Target.TIER_BLUE * Config.Target.TIER_BLUE * 2000)
//        drawEllipses(blue, Colors.COLOR_BLUE)
        for (e in blue) {
            for (t in potentialTargets) {
                t.fit(e, Config.Target.TIER_BLUE)
            }
        }
        val imgCenter = Point(img.size().width / 2, img.size().height / 2)
        val greaterThan = object : Comparator<FitTarget> {
            override fun compare(x: FitTarget, y: FitTarget): Int {
                if (x.fittedEllipses.size == y.fittedEllipses.size) {
                    val d1 = MatHelper.distance(x.center, imgCenter)
                    val d2 = MatHelper.distance(y.center, imgCenter)
                    return if (d1 > d2) -1 else (if (d1 < d2) 1 else 0)
                }
                return x.fittedEllipses.size - y.fittedEllipses.size
            }
        }

//        for (t in potentialTargets) {
//            if (t.fittedEllipses.size == 3) {
//                val points = Mat()
//                Imgproc.boxPoints(t.tierWhite, points)
////                println(points.dump())
//                drawEllipses(t.allRings())
//            }
//        }

//        Imgcodecs.imwrite("C:\\Users\\Adomas\\Documents\\Projects\\archinator\\target\\$name", img)

        val fitted = potentialTargets.filter { it.targetWithinImage() }.maxWith(greaterThan)!!
        bestFittedTarget = fitted

        val perspectiveImg = img.clone()
//         perspectiveTransform
        Imgproc.warpPerspective(img, perspectiveImg, fitted.perspective, Size(Config.Target.TARGET_IMAGE_SIZE, Config.Target.TARGET_IMAGE_SIZE))
        logger.log("perspective", perspectiveImg)
        logger.log("closest", closest)
//        Imgcodecs.imwrite("C:\\Users\\Adomas\\Documents\\Projects\\archinator\\target\\perspective-$name", perspectiveImg)
//        Imgcodecs.imwrite("C:\\Users\\Adomas\\Documents\\Projects\\archinator\\target\\closest-$name", closest)

//
//        val rings = fitted!!.allRings()
//        drawEllipses(rings)
//        for (e in blue) {
//            potentialTargets.add(FitTarget(e, TIER_BLUE))
//        }


//        val black = detectCircles(closest, Colors.COLOR_BLACK)
//        drawEllipses(black, Colors.COLOR_BLACK)


    }

    private fun detectCircles(closest: Mat, color: Color, minArea: Int = 2000): List<RotatedRect> {
        val dilationSize = 10.0

        val singleColor = MatHelper.extractColor(closest, color)
        val borders = Mat()

        Imgproc.cvtColor(singleColor, borders, Imgproc.COLOR_BGR2GRAY)
        val t = 20.0
        Imgproc.Canny(borders, borders, t, t * 3)
        val element = Imgproc.getStructuringElement(Imgproc.MORPH_CROSS, Size(dilationSize, dilationSize))
        Imgproc.dilate(borders, borders, element)

        val contours = mutableListOf<MatOfPoint>()
        val hierarchy = Mat()
        Imgproc.findContours(borders, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE)

        val ellipses = mutableListOf<RotatedRect>()

        for ((i, c) in contours.withIndex()) {


            val hull = MatOfInt()
            Imgproc.convexHull(c, hull)

            val hullPoints = MatHelper.convertIndexesToPoints(c, hull)

            val area = Imgproc.contourArea(hullPoints)

            if (area < minArea)
                continue

            val cnt = MatOfPoint2f()
            hullPoints.convertTo(cnt, CvType.CV_32F);

            val ellipse = Imgproc.fitEllipse(cnt)

            ellipse.size = Size(ellipse.size.width - dilationSize, ellipse.size.height - dilationSize)
            ellipses.add(ellipse)

//            val cntOrg = MatOfPoint2f()
//            c.convertTo(cntOrg, CvType.CV_32F);
//            val originalEllipse = Imgproc.fitEllipse(cntOrg)
//            originalEllipse.size = Size(originalEllipse.size.width - dilationSize, originalEllipse.size.height - dilationSize)
//            ellipses.add(originalEllipse)
        }
        return ellipses
    }


}