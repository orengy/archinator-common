package com.orengy.archinator.common

import com.orengy.archinator.common.logger.AbstractLogger
import com.orengy.archinator.common.logger.EmptyLogger
import org.opencv.calib3d.Calib3d
import org.opencv.core.*
import org.opencv.features2d.FeatureDetector
import org.opencv.imgcodecs.Imgcodecs
import org.opencv.imgproc.Imgproc
import org.opencv.features2d.Features2d
import org.opencv.core.MatOfPoint2f
import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.features2d.DescriptorExtractor
import org.opencv.features2d.DescriptorMatcher
import java.util.*


class ArrowsTarget(
        val img: Mat,
        val referenceTarget: ReferenceTarget,
        val targetDiameter: Double = 0.6,
        val arrowDiameterMm: Double = 1.0,
        val logger: AbstractLogger = EmptyLogger()) {

    val arrowDiameterPixels: Double = arrowDiameterMm * Config.Target.TARGET_RADIUS / (1000.0 * targetDiameter)

    val targetFinder = ReferenceTarget(img)//, logger)

    lateinit var referenceTargetPerspective: Mat
    lateinit var referenceTargetPerspectiveInverse: Mat
    lateinit var referenceAffineTransform: Mat

    init {


//        val sceneImage = referenceTarget.img
//        val sceneImage = referenceTarget.getTransformedTarget()
//        val objectImage = targetFinder.getTransformedTarget()
        val sceneImage = referenceTarget.img
        val objectImage = img


        val objectKeyPoints = MatOfKeyPoint()
        val featureDetector = FeatureDetector.create(FeatureDetector.ORB)
        featureDetector.detect(objectImage, objectKeyPoints)
        val keypoints = objectKeyPoints.toArray()

        val objectDescriptors = MatOfKeyPoint()
        val descriptorExtractor = DescriptorExtractor.create(DescriptorExtractor.ORB)
        descriptorExtractor.compute(objectImage, objectKeyPoints, objectDescriptors)

        val outputImage = Mat(objectImage.rows(), objectImage.cols(), Imgcodecs.CV_LOAD_IMAGE_COLOR)
        val newKeypointColor = Scalar(255.0, 0.0, 0.0)

        Features2d.drawKeypoints(objectImage, objectKeyPoints, outputImage, newKeypointColor, 0)

        val sceneKeyPoints = MatOfKeyPoint()
        val sceneDescriptors = MatOfKeyPoint()
        featureDetector.detect(sceneImage, sceneKeyPoints)
        descriptorExtractor.compute(sceneImage, sceneKeyPoints, sceneDescriptors)

        val outputImageScene = Mat(sceneImage.rows(), sceneImage.cols(), Imgcodecs.CV_LOAD_IMAGE_COLOR)
        val newKeypointColorScene = Scalar(255.0, 0.0, 0.0)

        Features2d.drawKeypoints(sceneImage, sceneKeyPoints, outputImageScene, newKeypointColorScene, 0)

        val matchoutput = Mat(sceneImage.rows() * 2, sceneImage.cols() * 2, Imgcodecs.CV_LOAD_IMAGE_COLOR)
        val matchestColor = Scalar(0.0, 255.0, 0.0)


        val matches = LinkedList<MatOfDMatch>()
        val descriptorMatcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE)
        descriptorMatcher.knnMatch(objectDescriptors, sceneDescriptors, matches, 2)

        val goodMatchesList = LinkedList<DMatch>()

        val nndrRatio = 0.7f

        for (i in matches.indices) {
            val matofDMatch = matches[i]
            val dmatcharray = matofDMatch.toArray()
            val m1 = dmatcharray[0]
            val m2 = dmatcharray[1]

            if (m1.distance <= m2.distance * nndrRatio) {
                goodMatchesList.addLast(m1)

            }
        }
        println(goodMatchesList.size)
        if (goodMatchesList.size >= 7) {

            val objKeypointlist = objectKeyPoints.toList()
            val scnKeypointlist = sceneKeyPoints.toList();

            val objectPoints = mutableListOf<Point>()
            val scenePoints = mutableListOf<Point>()


            for (i in 0 until goodMatchesList.size) {
                objectPoints.add(objKeypointlist[goodMatchesList[i].queryIdx].pt)
                scenePoints.add(scnKeypointlist[goodMatchesList[i].trainIdx].pt)
            }

            val objMatOfPoint2f = MatOfPoint2f()
            objMatOfPoint2f.fromList(objectPoints)
            val scnMatOfPoint2f = MatOfPoint2f()
            scnMatOfPoint2f.fromList(scenePoints)

            referenceTargetPerspective = Calib3d.findHomography(objMatOfPoint2f, scnMatOfPoint2f, Calib3d.RANSAC, 3.0)
            referenceTargetPerspectiveInverse = Calib3d.findHomography(scnMatOfPoint2f, objMatOfPoint2f, Calib3d.RANSAC, 3.0)

            val circlePoints = pointsAroundTheCircle(Config.Target.TARGET_CENTER, Config.Target.TARGET_RADIUS, 20)
            val angles = mutableListOf<Double>()
            for (p in circlePoints) {
                val tp = targetFinder.perspectivePointTransform(inversePerspectivePointTransform(p))
                val angle = LineHelper.angleBetweenVectors(
                        Point(
                                p.x - Config.Target.TARGET_CENTER.x,
                                p.y - Config.Target.TARGET_CENTER.y
                        ),
                        Point(
                                tp.x - Config.Target.TARGET_CENTER.x,
                                tp.y - Config.Target.TARGET_CENTER.y
                        )
                )
                angles.add(angle)
            }

            val avg = angles.average()
            val rotatedAngle = if (avg < 0) avg + 360 else avg
            val rot = Imgproc.getRotationMatrix2D(Config.Target.TARGET_CENTER, -rotatedAngle, 1.0)
            val found = targetFinder.getTransformedTarget()
            Imgproc.warpAffine(found, found, rot, found.size(), Imgproc.INTER_CUBIC);
            logger.log("rotated_alligned", found)

//            referenceAffineTransform = Calib3d.estimateAffine2D(objMatOfPoint2f, scnMatOfPoint2f)
//logger.log("whatup",getTransformedTarget())
//            val perspectiveImg = objectImage.clone()
//            Imgproc.warpPerspective(objectImage, perspectiveImg, referenceTargetPerspective, found.size())
            logger.log("wrapped", getTransformedTarget())
//
////                            val detectedTargetPerspective = target.bestFittedTarget.perspective.mul(homography)
//
//            Imgproc.warpPerspective(perspectiveImg, perspectiveImg, target.bestFittedTarget.perspective, Size(Target.TARGET_IMAGE_SIZE, Target.TARGET_IMAGE_SIZE))
//            logger.log("wrapped_target", perspectiveImg)
//
//
//            val closestWrapped = MatHelper.closestColors(perspectiveImg)
//            logger.log("wrapped_closest", closestWrapped)
//            val extractedBlack = MatHelper.extractColor(closestWrapped, Colors.COLOR_BLACK, 0.0)
//            logger.log("blackExtracted", extractedBlack)
//
//                            val edges = perspectiveImg.clone()
//                            Imgproc.medianBlur(edges, edges, 5)
//
//                            Imgproc.cvtColor(edges, edges, Imgproc.COLOR_BGR2GRAY)
//                            Imgproc.blur(edges, edges, Size(4.0, 4.0))
//                            val t = 20.0
//                            Imgproc.Canny(edges, edges, t, t * 3)
//                            val element = Imgproc.getStructuringElement(Imgproc.MORPH_CROSS, Size(3.0, 3.0))
//                            Imgproc.dilate(edges, edges, element)
//                            logger.log("edges", edges)


            val goodMatches = MatOfDMatch()
            goodMatches.fromList(goodMatchesList)
//
            Features2d.drawMatches(objectImage, objectKeyPoints, sceneImage, sceneKeyPoints, goodMatches, matchoutput, matchestColor, newKeypointColor, MatOfByte(), 2)

//            logger.log("matchOut", matchoutput)
        } else {
            println("HEEEEEY")
        }


//        logger.log("object", outputImage)
//        logger.log("scene", sceneImage)
//                        val sceneClosest = MatHelper.closestColors(sceneImage)
//                        logger.log("scene_closest", sceneClosest)
//                        return@main

    }

    fun pointsAroundTheCircle(center: Point, radius: Double, count: Int): List<Point> {
        val points = mutableListOf<Point>()
        for (i in 0..360 step 360 / count) {
            points.add(
                    Point(
                            center.x + radius * Math.cos(Math.toRadians(i.toDouble())),
                            center.y + radius * Math.sin(Math.toRadians(i.toDouble()))
                    )
            )
        }
        return points
    }

    fun isPointInTier(p: Point, t: Int): Boolean {
        val d = LineHelper.distanceBetweenPoints(p, Config.Target.TARGET_CENTER)
        val lb = (t - 1) * (Config.Target.TARGET_RADIUS / Config.Target.TIER_WHITE)
        val ub = (t) * (Config.Target.TARGET_RADIUS / Config.Target.TIER_WHITE)
        return d in lb..ub
    }

    fun pointScore(p: Point): Int {
        val d = Math.max(LineHelper.distanceBetweenPoints(p, Config.Target.TARGET_CENTER) - arrowDiameterPixels, 0.0)
        return Math.max(10 - Math.floor(d * 10 / Config.Target.TARGET_RADIUS).toInt(), 0)
    }

    fun pointToRealMetrics(p: Point): Point {
        val x = (p.x - Config.Target.TARGET_CENTER.x) * targetDiameter / Config.Target.TARGET_RADIUS
        val y = (p.y - Config.Target.TARGET_CENTER.y) * targetDiameter / Config.Target.TARGET_RADIUS

        return Point(x, y)
    }

    fun getTransformedTarget(): Mat {
        val perspectiveImg = img.clone()
        Imgproc.warpPerspective(img, perspectiveImg, referenceTargetPerspective, img.size())
        Imgproc.warpPerspective(perspectiveImg, perspectiveImg, referenceTarget.bestFittedTarget.perspective, Size(Target.TARGET_IMAGE_SIZE, Target.TARGET_IMAGE_SIZE))
//        Imgproc.warpPerspective(perspectiveImg, perspectiveImg, referenceTargetPerspective, perspectiveImg.size())
//        Imgproc.warpAffine(perspectiveImg, perspectiveImg, referenceAffineTransform, perspectiveImg.size())

        return perspectiveImg
    }

    fun pointOnTarget(p: Point): Boolean {
        val d = LineHelper.distanceBetweenPoints(p, Config.Target.TARGET_CENTER)
        return d <= Config.Target.TARGET_RADIUS
    }

    fun isLneTangentOfTargetCircles(p1: Point, p2: Point): Boolean {
//        return false
        //TODO: Improve function
        val d1 = LineHelper.distanceBetweenPoints(p1, Config.Target.TARGET_CENTER)
        val d2 = LineHelper.distanceBetweenPoints(p2, Config.Target.TARGET_CENTER)

        if (Math.abs(d1 - d2) < 5.0) {
            val r = (d1 + d2) / 2
            val midPoint = Point((p1.x + p2.x) / 2, (p1.y + p2.y) / 2)
            val d = LineHelper.distanceBetweenPoints(midPoint, Config.Target.TARGET_CENTER)
            return Math.abs(r - d) < 5.0
        }
        return false
    }

    fun perspectivePointTransform(p: Point): Point {
        val dst1 = Mat()
        val dst2 = Mat()
        val src = Mat(1, 1, CvType.CV_32FC2)
        src.put(0, 0, p.x, p.y)
        Core.perspectiveTransform(src, dst1, referenceTargetPerspective)
        Core.perspectiveTransform(dst1, dst2, referenceTarget.bestFittedTarget.perspective)
        return Point(dst2.get(0, 0)[0], dst2.get(0, 0)[1])
    }

    fun inversePerspectivePointTransform(p: Point): Point {
        val dst1 = Mat()
        val dst2 = Mat()
        val src = Mat(1, 1, CvType.CV_32FC2)
        src.put(0, 0, p.x, p.y)
        Core.perspectiveTransform(src, dst1, referenceTarget.bestFittedTarget.inversePerspective)
        Core.perspectiveTransform(dst1, dst2, referenceTargetPerspectiveInverse)
        return Point(dst2.get(0, 0)[0], dst2.get(0, 0)[1])
    }

}