package com.orengy.archinator.common

import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.core.Point

/**
 * Created by Adomas on 2017-12-18.
 */
object LineHelper {
    fun projectOnLine(p: Point, n: Point, a: Point): Point {
        val k = ((p.x - a.x) * n.x + (p.y - a.y) * n.y) / (n.x * n.x + n.y * n.y)
        return Point(a.x + k * n.x, a.y + k * n.y)

    }

    fun angleBetweenVectors(p1: Point, p2: Point): Double {
        val a = Math.toDegrees(Math.atan2(p1.y, p1.x) - Math.atan2(p2.y, p2.x))
        if (a > 180)
            return a - 360
        else
            return a
    }

    fun distanceBetweenPoints(p1: Point, p2: Point): Double {
        return Math.sqrt(Math.pow(p1.x - p2.x, 2.0) + Math.pow(p1.y - p2.y, 2.0))
    }

    fun normalizeVector(p: Point): Point {
        val l = Math.sqrt(p.x * p.x + p.y * p.y)
        return Point(p.x / l, p.y / l)
    }

    fun middlePoint(p1: Point, p2: Point): Point {
        return Point((p1.x + p2.x) / 2, (p1.y + p2.y) / 2)
    }

    fun perpendicularDistanceToLine(p: Point, n: Point, a: Point): Double {
        val ampx = a.x - p.x
        val ampy = a.y - p.y
        val ampdn = ampx * n.x + ampy * n.y
        val dx = ampx - ampdn * n.x
        val dy = ampy - ampdn * n.y
        val d = Math.sqrt(dx * dx + dy * dy)
        return d;
    }

    fun doLinesOverlap(n: Point, a: Point, p1: Point, p2: Point, p3: Point, p4: Point): Boolean {
        val distantPoint = Point(a.x + 10000 * n.x, a.y + 10000 * n.y)
        val d1 = LineHelper.distanceBetweenPoints(distantPoint, p1)
        val d2 = LineHelper.distanceBetweenPoints(distantPoint, p2)
        val d3 = LineHelper.distanceBetweenPoints(distantPoint, projectOnLine(p3, n, a))
        val d4 = LineHelper.distanceBetweenPoints(distantPoint, projectOnLine(p4, n, a))
        return !(Math.max(d1, d2) < Math.min(d3, d4) || Math.min(d1, d2) > Math.max(d3, d4))

    }

    fun centerLine(n1: Point, a1: Point, n2: Point, a2: Point): Pair<Point, Point> {
        val n = normalizeVector(Point(n1.x + n2.x, n1.y + n2.y))
//        val p1 = projectOnLine(a1, n2, a2)
//        val p2 = projectOnLine(a2, n1, a1)
//        val m1 = middlePoint(p1, a2)
//        val m2 = middlePoint(p2, a1)
//        val m = middlePoint(m1, m2)
//        return Pair(n, m)

        val rotatedN = Point(n.y, -n.x)

        val nMat = arrayOf(
                arrayOf(rotatedN.x, -n2.x),
                arrayOf(rotatedN.y, -n2.y)
        )
        val det = nMat[0][0] * nMat[1][1] - nMat[1][0] * nMat[0][1]
        val nMatInv = arrayOf(
                arrayOf(nMat[1][1] / det, -nMat[0][1] / det),
                arrayOf(-nMat[1][0] / det, nMat[0][0] / det)
        )
        val vMat = arrayOf(a2.x - a1.x, a2.y - a1.y)
        val res = arrayOf(
                nMatInv[0][0] * vMat[0] + nMatInv[0][1] * vMat[1],
                nMatInv[1][0] * vMat[0] + nMatInv[1][1] * vMat[1]
        )


        return Pair(n, middlePoint(a1, Point(a1.x + res[0] * rotatedN.x, a1.y + res[0] * rotatedN.y)))
    }

    fun infiniteLineToPoints(n: Point, a: Point): Pair<Point, Point> {
        return Pair(Point(a.x + n.x * 10000, a.y + n.y * 10000), Point(a.x - n.x * 10000, a.y - n.y * 10000))
    }

    fun vectorLength(v: Point): Double {
        return Math.sqrt(v.x * v.x + v.y * v.y)
    }
}