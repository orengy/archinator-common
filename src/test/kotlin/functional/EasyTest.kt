package functional

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.orengy.archinator.common.*
import com.orengy.archinator.common.Target
import com.orengy.archinator.common.model.TargetEnd
import org.junit.Test
import org.opencv.core.*
import org.opencv.imgcodecs.Imgcodecs
import org.opencv.imgproc.Imgproc
import org.opencv.core.Scalar
import org.opencv.core.Core
import java.awt.Color
import org.opencv.features2d.FeatureDetector
import org.opencv.core.MatOfKeyPoint
import org.opencv.core.KeyPoint








/**
 * Created by Adomas on 2017-11-21.
 */
class EasyTest {


    @Test
    fun calibrate() {


//        val objectKeyPoints = MatOfKeyPoint()
//        val featureDetector = FeatureDetector.create(FeatureDetector.ORB)
//        featureDetector.detect(file, objectKeyPoints)
//        val keypoints = objectKeyPoints.toArray()
//        println(keypoints.size)
//
//        return



        return
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME)
        val camera = Camera(9, 6)
        camera.calibrate("C:/Users/Adomas/Documents/Projects/Archinator/calibration")

//        println(camera.toJson())

        val file = Imgcodecs.imread("C:\\Users\\Adomas\\Documents\\Projects\\Archinator\\undistorted\\IMG_20171011_173140.jpg")
        val undistorted = Mat()
        camera.undistort(file, undistorted)
        Imgcodecs.imwrite("C:\\Users\\Adomas\\Documents\\Projects\\Archinator\\undistorted\\undistorted.jpg", undistorted)
    }

    @Test
    fun cv() {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME)

        val camera = Camera(9, 6)
        camera.fromJsonString("{\"intrinsic\":[828.2247325294277,0.0,389.62051037125826,0.0,830.5137032475825,519.4797297179107,0.0,0.0,1.0],\"dist_coeffs\":[0.31912292123540964,-1.6751591326152708,0.0012529359419450874,-3.685044689066447E-4,2.8946816702611944]}")
//        camera.calibrate("C:/Users/Adomas/Documents/Archinator/calibration")
//        camera.print()
        val gson = GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_DASHES).create()
        val type = object : TypeToken<List<TargetEnd>>() {}.type
        val easyStream = javaClass.classLoader.getResourceAsStream("easy.json")
        val ends = gson.fromJson<List<TargetEnd>>(Utils.streamToString(easyStream), type)
        easyStream.close()

        val fileInPath = "C:\\Users\\Adomas\\Documents\\Projects\\archinator\\in"
        val fileOutPath = "C:\\Users\\Adomas\\Documents\\Projects\\archinator\\out"

        var c = 0
//        val end = ends[0]
        for (end in ends) {
            c++
            val file = Imgcodecs.imread("$fileInPath/${end.file}");
            MatHelper.resize(file)


            val img = Mat()
            camera.undistort(file, img)

//            val target = Target(img, end.file)
//            target.detectTarget()

//            val closest = MatHelper.closestColors(target.getTransformedTarget())
//            val closest = MatHelper.closestColors(img)

//            val arrows = Arrows(closest, end.file)
//            val arrows = Arrows(img, end.file)
            val arrows = Arrows(img, end.file)
            val scoredArrows = arrows.findArrows()
//            println("=================")
//            println("Found arrows: ${scoredArrows.size}/${end.arrows.size}")
//            println("Sums:         ${scoredArrows.sumBy { it.score }}/${end.arrows.sumBy { it.score }}")
//            println("Sums diff:    ${scoredArrows.sumBy { it.score } - end.arrows.sumBy { it.score }}")

            println("${scoredArrows.size},${end.arrows.size},${scoredArrows.sumBy { it.score } - end.arrows.sumBy { it.score }},${end.file}")

//            if (c > 9)
                return
        }

    }

    @Test
    fun cvOnly() {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME)

        val camera = Camera(9, 6)
        camera.calibrate("C:/Users/Adomas/Documents/Archinator/calibration")

        val gson = GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_DASHES).create()
        val type = object : TypeToken<List<TargetEnd>>() {}.type
        val easyStream = javaClass.classLoader.getResourceAsStream("easy.json")
        val ends = gson.fromJson<List<TargetEnd>>(Utils.streamToString(easyStream), type)
        easyStream.close()

        val fileInPath = "C:/Users/Adomas/Documents/Archinator/in"
        val fileOutPath = "C:/Users/Adomas/Documents/Archinator/out"

        var c = 0
//        val end = ends[0]
        for (end in ends) {
            c++
            val file = Imgcodecs.imread("$fileInPath/${end.file}");

            Imgproc.resize(file, file, Size(768.0, 1024.0))

//            val yellow = extractColor(file, 251, 212, 0, 40.0)
//            val blue = extractColor(file, 0, 158, 206, 30.0)
//            val red = extractColor(file, 217, 25, 25, 40.0)
//
//            yellow.copyTo(blue, yellow)
//            red.copyTo(blue, red)
//            Imgcodecs.imwrite("$fileOutPath/yellow-${end.file}", yellow)
//            Imgcodecs.imwrite("$fileOutPath/blue-${end.file}", blue)


            val closest = closestColors(file)

//            val yellow = extractColor(closest, 255, 235, 59, 1.0)
//            Imgcodecs.imwrite("$fileOutPath/yellow-only-${end.file}", yellow)
//
//            return


            Imgproc.medianBlur(closest, closest, 5)

            Imgcodecs.imwrite("$fileOutPath/closest-${end.file}", closest)


            Imgproc.cvtColor(closest, file, Imgproc.COLOR_BGR2GRAY)
            Imgproc.blur(file, file, Size(4.0, 4.0))
            val t = 20.0
            Imgproc.Canny(file, file, t, t * 3)
            val element = Imgproc.getStructuringElement(Imgproc.MORPH_CROSS, Size(3.0, 3.0))
            Imgproc.dilate(file, file, element)

            val lines = Mat()

            Imgproc.HoughLinesP(file, lines, 1.0, Math.PI / 180, 100, 10.0, 5.0)

            Imgproc.cvtColor(file, file, Imgproc.COLOR_GRAY2BGR)
            for (i in 0 until lines.rows()) {
                val vec = lines.get(i, 0)
                val x1 = vec[0]
                val y1 = vec[1]
                val x2 = vec[2]
                val y2 = vec[3]

                Imgproc.line(file, Point(x1, y1), Point(x2, y2), Scalar(255.0 * (i % 2), 255.0 * ((i + 1) % 2), 0.0), 3)

            }

            Imgcodecs.imwrite("$fileOutPath/${end.file}", file)
//            return
            if (c > 5)
                return
        }
    }

    fun extractColor(src: Mat, r: Int, g: Int, b: Int, threshold: Double): Mat {
        val res = src.clone()
        val target = LAB.fromRGB(r, g, b, 0.0)

        for (i in 0 until res.rows()) {
            for (j in 0 until res.cols()) {
                val color = res[i, j]
                val current = LAB.fromRGB(color[2].toInt(), color[1].toInt(), color[0].toInt(), 0.0)
                if (target.distance(current) < threshold) {
                    res.put(i, j, b.toDouble(), g.toDouble(), r.toDouble())
                } else {
                    res.put(i, j, 0.0, 0.0, 0.0)
                }
            }
        }
        return res
    }

    fun closestColors(src: Mat): Mat {
        val res = src.clone()

        val colors = listOf<LAB>(
                LAB.fromRGB(255, 255, 255, 0.0),
//                LAB.fromRGB(158, 158, 158, 0.0), Grey
                LAB.fromRGB(244, 67, 54, 0.0),
                LAB.fromRGB(0, 0, 0, 0.0),
//                LAB.fromRGB(233, 30, 99, 0.0), Purple
                LAB.fromRGB(156, 39, 176, 0.0),
                LAB.fromRGB(33, 150, 243, 0.0),
                LAB.fromRGB(76, 175, 80, 0.0),
                LAB.fromRGB(255, 235, 59, 0.0)
//                LAB.fromRGB(255, 152, 0, 0.0)
        )

        for (i in 0 until res.rows()) {
            for (j in 0 until res.cols()) {
                val color = res[i, j]
                val current = LAB.fromRGB(color[2].toInt(), color[1].toInt(), color[0].toInt(), 0.0)

                val closest = Color(colors.map { Pair(it, current.distance(it)) }.minBy { it.second }!!.first.rgb())
                res.put(i, j, closest.blue.toDouble(), closest.green.toDouble(), closest.red.toDouble())
            }
        }
        return res
    }
}